﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQl_connet.empleados;
using System.Data;
using System.Data.SqlClient;
namespace SQl_connet.empleados
{
    public class Pagos_y_vales:Class1
    {
        public List<string> lista_mostrar(string tabla)
        {
            List<string> lista = new List<string>();

            miconexion.Open();
            SqlCommand cd = new SqlCommand(tabla, miconexion);
            SqlDataReader registro = cd.ExecuteReader();

            while (registro.Read())
            {
                var myString = Convert.ToString(registro.GetValue(0));
                lista.Add(myString);

            }
            miconexion.Close();
            return lista;
        }

        public int insertar(string nombre, string Motivo, string Fecha, string monto,string cedula) { 
            miconexion.Open();
            string query = "insert into TablaPagos_y_Vales(cedula,Nombre,Motivo,fecha,Monto)"
                           + "values(@cedula,@nombre,@motivo,@fecha,@monto)";
            SqlCommand cd = new SqlCommand(query, miconexion);
            cd.Parameters.AddWithValue("@cedula", cedula);
            cd.Parameters.AddWithValue("@nombre", nombre);
            cd.Parameters.AddWithValue("@motivo",Motivo);
            cd.Parameters.AddWithValue("@fecha",Fecha);
            cd.Parameters.AddWithValue("@monto",monto);
            int nueva_fila = cd.ExecuteNonQuery();
            miconexion.Close();
            return nueva_fila;

        }
    }
}
