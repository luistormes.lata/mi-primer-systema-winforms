﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace SQl_connet.empleados
{
    public class Datos_Empleados:Class1
    {
        public int insertar(string nombre, string cedula, string telefono, string cargo,string salario)
        {
            miconexion.Open();
            string query = "insert into Empleados(Nombre,cedula,Telefono,cargo,sueldo)"
                           + "values(@nombre,@cedula,@telefono,@cargo,@salario)";
            SqlCommand cd = new SqlCommand(query, miconexion);
            cd.Parameters.AddWithValue("@nombre", nombre);
            cd.Parameters.AddWithValue("@cedula", cedula);
            cd.Parameters.AddWithValue("@telefono", telefono);
            cd.Parameters.AddWithValue("@cargo", cargo);
            cd.Parameters.AddWithValue("@salario",salario);

            int nueva_fila = cd.ExecuteNonQuery();

            miconexion.Close();
            return nueva_fila;

        }
    }
}
