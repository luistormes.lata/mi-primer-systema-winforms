﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace SQl_connet.ventas
{
    public class Facturar:Class1
    {
        
        public bool insertar_venta(string codigo, string cliente, int cantidad, string producto, string fecha,string referencia,int monto,string T_pago,string Cedula)
        {

            miconexion.Open();

            string query = "insert into Tabla_Ventas (" +
                                "codigo_de_venta," +
                                "cedula," +
                                "Nombre_cliente," +
                                "Cant_Productos," +
                                "Productos," +
                                "Fecha," +
                                "Referencia," +
                                "Monto," +
                                "Tipo_de_pago) " +
                                
                            "values (" +
                                "@codigo," +
                                "@cedula," +
                                "@name," +
                                "@cp," +
                                "@productos," +
                                "@fecha," +
                                "@referencia," +
                                "@monto," +
                                "@T_Pago)";
            SqlCommand cd = new SqlCommand(query, miconexion);
            cd.Parameters.AddWithValue("@codigo",codigo);
            cd.Parameters.AddWithValue("@cedula",Cedula);
            cd.Parameters.AddWithValue("@name",cliente);
            cd.Parameters.AddWithValue("@cp",cantidad);
            cd.Parameters.AddWithValue("@productos",producto);
            cd.Parameters.AddWithValue("@fecha",fecha);
            cd.Parameters.AddWithValue("@referencia",referencia);
            cd.Parameters.AddWithValue("@monto",monto);
            cd.Parameters.AddWithValue("@T_pago",T_pago);
            int Nuevafila = cd.ExecuteNonQuery();

            miconexion.Close();
            if (Nuevafila > 0) 
                return true;
            else 
                return false;

        }
    }
}
