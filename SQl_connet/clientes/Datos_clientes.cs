﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQl_connet.clientes
{
    public class Datos_clientes:Class1
    {
        public int  insertar(string nombre, string apellido, string cedula, string telefono, string cargo)
        {
            miconexion.Open();
            string query = "insert into clientes(Nombre,Apellido,Cedula,Telefono,cargo)"
                           + "values(@nombre,@apellido,@cedula,@telefono,@cargo)";
            SqlCommand cd = new SqlCommand(query,miconexion);
            cd.Parameters.AddWithValue("@nombre",nombre);
            cd.Parameters.AddWithValue("@apellido",apellido);
            cd.Parameters.AddWithValue("@cedula",cedula);
            cd.Parameters.AddWithValue("@telefono",telefono);
            cd.Parameters.AddWithValue("@cargo",cargo);
            int nueva_fila = cd.ExecuteNonQuery();
           
            miconexion.Close();
            return nueva_fila;

        }

        public List<string> lista_mostrar(string tabla)
        {
            List<string> lista = new List<string>();

            miconexion.Open();
            SqlCommand cd = new SqlCommand(tabla, miconexion);
            SqlDataReader registro = cd.ExecuteReader();

            while (registro.Read())
            {
                var myString = Convert.ToString(registro.GetValue(0));
                lista.Add(myString);

            }
            miconexion.Close();
            return lista;
        }

    }
}
