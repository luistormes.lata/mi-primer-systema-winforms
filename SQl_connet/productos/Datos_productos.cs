﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace SQl_connet.productos
{
    public class Datos_productos:Class1
    {
        
        public int insertar(string nombre,string precio)
        {
            miconexion.Open();
            string query = "insert into Productos(Nombre,Precio)" +
                           "values(@nombre,@precio)";
            SqlCommand cd = new SqlCommand(query,miconexion);
            cd.Parameters.AddWithValue("@nombre", nombre);
            cd.Parameters.AddWithValue("@precio", precio);
            int nueva_fila = cd.ExecuteNonQuery();

            miconexion.Close();
            return nueva_fila;
        }
    }
}
