﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace SQl_connet.gastos
{
    public class Datos_gastos:Class1
    {
        public List<string> lista_mostrar(string tabla)
        {
            List<string> lista = new List<string>();

            miconexion.Open();
            SqlCommand cd = new SqlCommand(tabla, miconexion);
            SqlDataReader registro = cd.ExecuteReader();

            while (registro.Read())
            {
                var myString = Convert.ToString(registro.GetValue(0));
                lista.Add(myString);

            }
            miconexion.Close();
            return lista;
        }
        public int insertar(string Tipo, string fecha, string Motivo, string DEscri, string monto)
        {
            miconexion.Open();
            string query = "insert into Gastos(Tipo,fecha,Motivo,Descripcion,Monto)"
                           + "values(@Tipo,@fecha,@motivo,@des,@monto)";
            SqlCommand cd = new SqlCommand(query, miconexion);
            cd.Parameters.AddWithValue("@Tipo",Tipo);
            cd.Parameters.AddWithValue("@fecha",fecha);
            cd.Parameters.AddWithValue("@motivo",Motivo);
            cd.Parameters.AddWithValue("@des", DEscri);
            cd.Parameters.AddWithValue("@monto", monto);
            int nueva_fila = cd.ExecuteNonQuery();

            miconexion.Close();
            return nueva_fila;

        }

    }
}
