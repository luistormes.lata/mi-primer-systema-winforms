﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SQl_connet.empleados;

namespace principal.Empleados
{
    
    public class logica_Empleados
    {
        Datos_Empleados ModeloEmpleado = new Datos_Empleados();

        public DataTable mostrar(string query)
        {
            return ModeloEmpleado.mostrar(query);
        }

        public string insertar(string nombre, string cedula, string telefono, string cargo,string salario)
        {

            if ((nombre != "") && (salario != "") && (cedula != "") && (telefono != ""))
            {
                int num = ModeloEmpleado.insertar(nombre,cedula, telefono, cargo,salario);

                if (num == 1)
                    return "se ha insertado correptamente";
                else
                    return "Ha ocurrido un error al tratar de insertar";
            }
            else
            {
                return "Faltan datos para insertar al Empleado";
            }
        }
       
        public string delete(string query)
        {
            int num = ModeloEmpleado.borrar_registro(query);
            if (num == 1)
                return "se ha eliminado correptamente";
            else
                return "ha ocurrido un error";
        }
        public string update(string query)
        {
            int num = ModeloEmpleado.actualizar(query);
            if (num == 1)
                return "se ha actualizado correptamente";
            else
                return "ha ocurrido un error";
        }
    }
}
