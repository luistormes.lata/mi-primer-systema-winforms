﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using SQl_connet.gastos;

namespace principal.GAstos
{
    public class Logica_gastos
    {
        Datos_gastos objeto = new Datos_gastos();

        public DataTable mostar(string query)
        {
            return objeto.mostrar(query);
        }
        public string insertar(string Tipo, string fecha, string Motivo, string DEscri, string monto)
        {
            if ((Tipo!= "") && (fecha != "") && (Motivo != "") && (DEscri != "") && (monto != ""))
            {
                int num = objeto.insertar(Tipo, fecha, Motivo,DEscri,monto) ;

                if (num == 1)
                    return "se ha insertado correptamente";
                else
                    return "Ha ocurrido un error al tratar de insertar";
            }
            else
            {
                return "Faltan datos para insertar el gasto";
            }
        }
        public List<string> mostrar_lista(string query)
        {
            return objeto.lista_mostrar(query);
        }
        public DataTable mostrar_por_periodo(string fechainicio, string fechafinal)
        {
            string tabla = "select Tipo,Motivo,Monto,fecha ,Descripcion from Gastos where fecha  between '" + fechainicio + " 'and '" + fechafinal + "'";
           
            return objeto.mostrar(tabla);
        }
    }
}
