﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using SQl_connet.clientes;

namespace principal.clientes
{
    public class logica_clientes
    {
        Datos_clientes ModeloCliente = new Datos_clientes();

        public DataTable mostrar(string tabla)
        {
           return ModeloCliente.mostrar(tabla);
        }

        public string insertar(string nombre,string apellido,string cedula,string telefono,string cargo)
        {

            if ((nombre !="")&&(apellido!="")&&(cedula!="")&&(telefono!=""))
            {
                int num = ModeloCliente.insertar(nombre, apellido, cedula, telefono, cargo);

                if (num == 1)
                    return "se ha insertado correptamente";
                else
                    return "Ha ocurrido un error al tratar de insertar";
            }
            else
            {
                return "Faltan datos para insertar al cliente";
            }
        }
        
        //actividad del cliente

        public List<string> mostrar_lista(string tabla)
        {
            return ModeloCliente.lista_mostrar(tabla);
        }
        public string delete(string query)
        {
            int num = ModeloCliente.borrar_registro(query);
            if (num == 1)
                return "se ha eliminado correptamente";
            else
                return "ha ocurrido un error";
        }
        public string update(string query)
        {
            int num = 0;
            num = ModeloCliente.actualizar(query);
            if (num ==1)
                return "se ha actualizado correptamente";
            
            else
                return "ha ocurrido un error";
        }

    }
}
