﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using SQl_connet.reportes;

namespace principal.reportes
{
    public class logica_reportes
    {
        Datos_reportes modeloreportes = new Datos_reportes();
        private DataTable dt;
        public List<string> Reporte_Registros()
        {
            string query = "select"+
            "(select count(*)  from productos) as p,"+
            "(select count(*)  from clientes) as cl,"+
            "(select count(*) from TablaUsuarios )as u,"+
            "(select count(*) from empleados) as e";


            List<string> lista_totales = new List<string>();
            dt = new DataTable();
            dt = modeloreportes.mostrar(query);
            foreach (DataRow fila in dt.Rows)
            {
                lista_totales.Add(fila["p"].ToString());
                lista_totales.Add(fila["cl"].ToString());
                lista_totales.Add(fila["u"].ToString());
                lista_totales.Add(fila["e"].ToString());
            }

            return lista_totales; 
        }
  
     
        public DataTable mostrar(string query)
        {
            return modeloreportes.mostrar(query);
        }
    }
}
