﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using SQl_connet.productos;

namespace principal.productos
{
    public class logica_producto
    {
        Datos_productos modeloproducto = new Datos_productos();

        public DataTable mostrar(string tabla)
        {
            return modeloproducto.mostrar(tabla);
        }

        public string insertar(string nombre,string precio)
        {

            if ((nombre != "") && (precio != "") )
            {
                int num = modeloproducto.insertar(nombre,precio);

                if (num == 1)
                    return "se ha insertado correptamente";
                else
                    return "Ha ocurrido un error al tratar de insertar";
            }
            else
            {
                return "Faltan datos para insertar el producto";
            }
        }

        public string delete(string query)
        {
            int num = modeloproducto.borrar_registro(query);
            if (num == 1)
                return "se ha eliminado correptamente";
            else
                return "ha ocurrido un error";
        }
        public string update(string query)
        {
            int num = modeloproducto.actualizar(query);
            if (num == 1)
                return "se ha actualizado correptamente";
            else
                return "ha ocurrido un error";
        }
    }
}
