﻿using Capa_UI.Formularios.Clientes;
using Capa_UI.Formularios.Empleados;
using Capa_UI.Formularios.Reportes;
using Capa_UI.Formularios.sistema;
using Capa_UI.Formularios.ventas;
using Capa_UI.Formularios.Gastos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Capa_UI.Formularios.Productos;

namespace Capa_UI
{
    public partial class Form1 : Form
    {
        public Form1(string usuario)
        {
            
            InitializeComponent();
            label_nombre.Text = usuario;
            label_fecha.Text = DateTime.Today.ToLongDateString();
        }
        //esta funcion cierra el login
       
        private void Barra_de_titulo_Paint(object sender, PaintEventArgs e)
        {

        }
        int posx = 0 , posy = 0;

        private void Panel_Menu_MouseMove(object sender, MouseEventArgs e)
        {
            if (MouseButtons.Left != e.Button)
            {
                posx = e.X;
                posy = e.Y;
            }
            else
            {
                Left = Left + (e.X - posx);
                Top = Top + (e.Y - posy);
            }
        }

        private void panel_Principal_MouseMove(object sender, MouseEventArgs e)
        {
            if (MouseButtons.Left != e.Button)
            {
                posx = e.X;
                posy = e.Y;
            }
            else
            {
                Left = Left + (e.X - posx);
                Top = Top + (e.Y - posy);
            }
        }

        private void Boton_Salir_Click(object sender, EventArgs e)
        {
            
            Application.Exit();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }
        int lx, ly;
        int sw, sh;

        private void label_nombre_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button_maximizar_Click(object sender, EventArgs e)
        {
            this.Size = new Size(sw, sh);
            this.Location = new Point(lx, ly);
            button_normal.Visible = true;
            button_maximizar.Visible = false;

        }

        private void button_normal_Click(object sender, EventArgs e)
        {
            lx = this.Location.X;
            ly = this.Location.Y;
            sw = this.Size.Width;
            sh = this.Size.Height;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            button_normal.Visible = false;
            button_maximizar.Visible = true;
        }

        private void button9_Click(object sender, EventArgs e)
        {
            formulario_hijo(new sistema());
        }

        private void btn_Ventas_Click(object sender, EventArgs e)
        {
            formulario_hijo(new Formulario_ventas());
        }

        private void button4_Click(object sender, EventArgs e)
        {
            formulario_hijo(new formulario_cliente());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            formulario_hijo(new Formulario_gastos() );
        }

        private void button6_Click(object sender, EventArgs e)
        {
            formulario_hijo(new formulario_empleados());
        }

        private void button7_Click(object sender, EventArgs e)
        {
            formulario_hijo(new formulario_Reportes());
        }

        private void btn_productos_Click(object sender, EventArgs e)
        {
            formulario_hijo(new Formulario_producto());
        }

        private void Mover_ventana(object sender, MouseEventArgs e)
        {
            if (MouseButtons.Left != e.Button)
            {
                posx = e.X;
                posy = e.Y;
            }
            else
            {
                Left = Left + (e.X - posx);
                Top = Top + (e.Y - posy);
            }
        }
        private Form formularioactivo = null;
        private void formulario_hijo(Form hijo)
        {
            if (formularioactivo != null)
            {
                formularioactivo.Close();
            }
            formularioactivo = hijo;
            hijo.TopLevel = false;
            hijo.FormBorderStyle = FormBorderStyle.None;
            hijo.Dock = DockStyle.Fill;
            panel_Principal.Controls.Add(hijo);
            panel_Principal.Tag = hijo;
            hijo.BringToFront();
            hijo.Show();
        }
    }
}
