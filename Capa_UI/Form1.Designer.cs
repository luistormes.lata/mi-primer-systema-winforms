﻿namespace Capa_UI
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.panel_fondo_base = new System.Windows.Forms.Panel();
            this.panel_Principal = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panel_usuario = new System.Windows.Forms.Panel();
            this.label_fecha = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label_nombre = new System.Windows.Forms.Label();
            this.Barra_de_titulo = new System.Windows.Forms.Panel();
            this.Boton_Salir = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.button_maximizar = new System.Windows.Forms.Button();
            this.button_normal = new System.Windows.Forms.Button();
            this.Panel_Menu = new System.Windows.Forms.Panel();
            this.btn_productos = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.btn_Ventas = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.panel_fondo_base.SuspendLayout();
            this.panel_Principal.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel_usuario.SuspendLayout();
            this.Barra_de_titulo.SuspendLayout();
            this.Panel_Menu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel_fondo_base
            // 
            this.panel_fondo_base.BackColor = System.Drawing.SystemColors.MenuHighlight;
            this.panel_fondo_base.Controls.Add(this.panel_Principal);
            this.panel_fondo_base.Controls.Add(this.Barra_de_titulo);
            this.panel_fondo_base.Controls.Add(this.Panel_Menu);
            this.panel_fondo_base.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_fondo_base.Location = new System.Drawing.Point(0, 0);
            this.panel_fondo_base.Name = "panel_fondo_base";
            this.panel_fondo_base.Size = new System.Drawing.Size(1017, 552);
            this.panel_fondo_base.TabIndex = 0;
            // 
            // panel_Principal
            // 
            this.panel_Principal.BackColor = System.Drawing.SystemColors.Menu;
            this.panel_Principal.Controls.Add(this.pictureBox1);
            this.panel_Principal.Controls.Add(this.panel_usuario);
            this.panel_Principal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_Principal.Location = new System.Drawing.Point(166, 27);
            this.panel_Principal.Name = "panel_Principal";
            this.panel_Principal.Size = new System.Drawing.Size(851, 525);
            this.panel_Principal.TabIndex = 1;
            this.panel_Principal.MouseMove += new System.Windows.Forms.MouseEventHandler(this.panel_Principal_MouseMove);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(3, 81);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(842, 253);
            this.pictureBox1.TabIndex = 5;
            this.pictureBox1.TabStop = false;
            // 
            // panel_usuario
            // 
            this.panel_usuario.BackColor = System.Drawing.SystemColors.ButtonFace;
            this.panel_usuario.Controls.Add(this.label_fecha);
            this.panel_usuario.Controls.Add(this.label1);
            this.panel_usuario.Controls.Add(this.label_nombre);
            this.panel_usuario.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel_usuario.Location = new System.Drawing.Point(0, 472);
            this.panel_usuario.Name = "panel_usuario";
            this.panel_usuario.Size = new System.Drawing.Size(851, 53);
            this.panel_usuario.TabIndex = 4;
            // 
            // label_fecha
            // 
            this.label_fecha.AutoSize = true;
            this.label_fecha.Font = new System.Drawing.Font("Segoe UI Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_fecha.Location = new System.Drawing.Point(566, 11);
            this.label_fecha.Name = "label_fecha";
            this.label_fecha.Size = new System.Drawing.Size(60, 25);
            this.label_fecha.TabIndex = 2;
            this.label_fecha.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(27, 11);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 25);
            this.label1.TabIndex = 1;
            this.label1.Text = "Usuario : ";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // label_nombre
            // 
            this.label_nombre.AutoSize = true;
            this.label_nombre.Font = new System.Drawing.Font("Segoe UI Light", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_nombre.Location = new System.Drawing.Point(118, 11);
            this.label_nombre.Name = "label_nombre";
            this.label_nombre.Size = new System.Drawing.Size(60, 25);
            this.label_nombre.TabIndex = 0;
            this.label_nombre.Text = "label1";
            this.label_nombre.Click += new System.EventHandler(this.label_nombre_Click);
            // 
            // Barra_de_titulo
            // 
            this.Barra_de_titulo.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(100)))), ((int)(((byte)(35)))), ((int)(((byte)(100)))));
            this.Barra_de_titulo.Controls.Add(this.Boton_Salir);
            this.Barra_de_titulo.Controls.Add(this.button1);
            this.Barra_de_titulo.Controls.Add(this.button_maximizar);
            this.Barra_de_titulo.Controls.Add(this.button_normal);
            this.Barra_de_titulo.Dock = System.Windows.Forms.DockStyle.Top;
            this.Barra_de_titulo.Location = new System.Drawing.Point(166, 0);
            this.Barra_de_titulo.Name = "Barra_de_titulo";
            this.Barra_de_titulo.Size = new System.Drawing.Size(851, 27);
            this.Barra_de_titulo.TabIndex = 1;
            this.Barra_de_titulo.Paint += new System.Windows.Forms.PaintEventHandler(this.Barra_de_titulo_Paint);
            this.Barra_de_titulo.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Mover_ventana);
            // 
            // Boton_Salir
            // 
            this.Boton_Salir.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.Boton_Salir.FlatAppearance.BorderSize = 0;
            this.Boton_Salir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Boton_Salir.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Boton_Salir.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.Boton_Salir.Location = new System.Drawing.Point(811, 2);
            this.Boton_Salir.Name = "Boton_Salir";
            this.Boton_Salir.Size = new System.Drawing.Size(37, 23);
            this.Boton_Salir.TabIndex = 0;
            this.Boton_Salir.Text = "X";
            this.Boton_Salir.UseVisualStyleBackColor = true;
            this.Boton_Salir.Click += new System.EventHandler(this.Boton_Salir_Click);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button1.Location = new System.Drawing.Point(747, -5);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(37, 31);
            this.button1.TabIndex = 3;
            this.button1.Text = "___";
            this.button1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button_maximizar
            // 
            this.button_maximizar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_maximizar.FlatAppearance.BorderSize = 0;
            this.button_maximizar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_maximizar.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_maximizar.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button_maximizar.Location = new System.Drawing.Point(779, -2);
            this.button_maximizar.Name = "button_maximizar";
            this.button_maximizar.Size = new System.Drawing.Size(37, 23);
            this.button_maximizar.TabIndex = 1;
            this.button_maximizar.Text = "¬";
            this.button_maximizar.UseVisualStyleBackColor = true;
            this.button_maximizar.Visible = false;
            this.button_maximizar.Click += new System.EventHandler(this.button_maximizar_Click);
            // 
            // button_normal
            // 
            this.button_normal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_normal.FlatAppearance.BorderSize = 0;
            this.button_normal.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button_normal.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button_normal.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button_normal.Location = new System.Drawing.Point(779, 2);
            this.button_normal.Name = "button_normal";
            this.button_normal.Size = new System.Drawing.Size(37, 23);
            this.button_normal.TabIndex = 2;
            this.button_normal.Text = "[]";
            this.button_normal.UseVisualStyleBackColor = true;
            this.button_normal.Click += new System.EventHandler(this.button_normal_Click);
            // 
            // Panel_Menu
            // 
            this.Panel_Menu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(28)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.Panel_Menu.Controls.Add(this.btn_productos);
            this.Panel_Menu.Controls.Add(this.button9);
            this.Panel_Menu.Controls.Add(this.button7);
            this.Panel_Menu.Controls.Add(this.button6);
            this.Panel_Menu.Controls.Add(this.button5);
            this.Panel_Menu.Controls.Add(this.btn_Ventas);
            this.Panel_Menu.Controls.Add(this.button4);
            this.Panel_Menu.Dock = System.Windows.Forms.DockStyle.Left;
            this.Panel_Menu.Location = new System.Drawing.Point(0, 0);
            this.Panel_Menu.Name = "Panel_Menu";
            this.Panel_Menu.Size = new System.Drawing.Size(166, 552);
            this.Panel_Menu.TabIndex = 0;
            this.Panel_Menu.MouseMove += new System.Windows.Forms.MouseEventHandler(this.Panel_Menu_MouseMove);
            // 
            // btn_productos
            // 
            this.btn_productos.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.btn_productos.FlatAppearance.BorderSize = 0;
            this.btn_productos.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_productos.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_productos.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_productos.Image = ((System.Drawing.Image)(resources.GetObject("btn_productos.Image")));
            this.btn_productos.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_productos.Location = new System.Drawing.Point(3, 232);
            this.btn_productos.Name = "btn_productos";
            this.btn_productos.Size = new System.Drawing.Size(154, 41);
            this.btn_productos.TabIndex = 8;
            this.btn_productos.Text = "     Productos";
            this.btn_productos.UseVisualStyleBackColor = false;
            this.btn_productos.Click += new System.EventHandler(this.btn_productos_Click);
            // 
            // button9
            // 
            this.button9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button9.Image = ((System.Drawing.Image)(resources.GetObject("button9.Image")));
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button9.Location = new System.Drawing.Point(3, 499);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(154, 41);
            this.button9.TabIndex = 7;
            this.button9.Text = "     Sistema";
            this.button9.UseVisualStyleBackColor = false;
            this.button9.Click += new System.EventHandler(this.button9_Click);
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button7.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button7.Image = ((System.Drawing.Image)(resources.GetObject("button7.Image")));
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(3, 279);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(154, 41);
            this.button7.TabIndex = 5;
            this.button7.Text = "Reportes";
            this.button7.UseVisualStyleBackColor = false;
            this.button7.Click += new System.EventHandler(this.button7_Click);
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button6.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button6.Image = ((System.Drawing.Image)(resources.GetObject("button6.Image")));
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(6, 185);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(154, 41);
            this.button6.TabIndex = 4;
            this.button6.Text = "      Empleados";
            this.button6.UseVisualStyleBackColor = false;
            this.button6.Click += new System.EventHandler(this.button6_Click);
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button5.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button5.Image = ((System.Drawing.Image)(resources.GetObject("button5.Image")));
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(6, 138);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(154, 41);
            this.button5.TabIndex = 3;
            this.button5.Text = "Gastos";
            this.button5.UseVisualStyleBackColor = false;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // btn_Ventas
            // 
            this.btn_Ventas.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.btn_Ventas.FlatAppearance.BorderSize = 0;
            this.btn_Ventas.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btn_Ventas.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btn_Ventas.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btn_Ventas.Image = ((System.Drawing.Image)(resources.GetObject("btn_Ventas.Image")));
            this.btn_Ventas.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btn_Ventas.Location = new System.Drawing.Point(6, 44);
            this.btn_Ventas.Name = "btn_Ventas";
            this.btn_Ventas.Size = new System.Drawing.Size(154, 41);
            this.btn_Ventas.TabIndex = 2;
            this.btn_Ventas.Text = " Ventas";
            this.btn_Ventas.UseVisualStyleBackColor = false;
            this.btn_Ventas.Click += new System.EventHandler(this.btn_Ventas_Click);
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(28)))), ((int)(((byte)(28)))));
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button4.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.button4.Image = ((System.Drawing.Image)(resources.GetObject("button4.Image")));
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(6, 91);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(154, 41);
            this.button4.TabIndex = 1;
            this.button4.Text = "Clientes";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 552);
            this.Controls.Add(this.panel_fondo_base);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.panel_fondo_base.ResumeLayout(false);
            this.panel_Principal.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel_usuario.ResumeLayout(false);
            this.panel_usuario.PerformLayout();
            this.Barra_de_titulo.ResumeLayout(false);
            this.Panel_Menu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel_fondo_base;
        private System.Windows.Forms.Panel panel_Principal;
        private System.Windows.Forms.Panel Barra_de_titulo;
        private System.Windows.Forms.Panel Panel_Menu;
        private System.Windows.Forms.Button Boton_Salir;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button_normal;
        private System.Windows.Forms.Button button_maximizar;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button btn_Ventas;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Panel panel_usuario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label_nombre;
        private System.Windows.Forms.Button btn_productos;
        private System.Windows.Forms.Label label_fecha;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}

