﻿namespace Capa_UI.Formularios.Empleados
{
    partial class formulario_empleados
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl_menu = new System.Windows.Forms.TabControl();
            this.Usuarios = new System.Windows.Forms.TabPage();
            this.textBox_Nombre = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.usuario = new System.Windows.Forms.GroupBox();
            this.button_update = new System.Windows.Forms.Button();
            this.textBox_salario = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.textBox_profecion = new System.Windows.Forms.TextBox();
            this.button_limpiar = new System.Windows.Forms.Button();
            this.textBox_telefono = new System.Windows.Forms.TextBox();
            this.textBox_cedula = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.button_agregar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button_editar = new System.Windows.Forms.Button();
            this.button_eliminR = new System.Windows.Forms.Button();
            this.dataGridView_Empleados = new System.Windows.Forms.DataGridView();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.comboBox_filtrar = new System.Windows.Forms.ComboBox();
            this.dataGridView_registro_general = new System.Windows.Forms.DataGridView();
            this.dateTimePicker_general_final = new System.Windows.Forms.DateTimePicker();
            this.button4 = new System.Windows.Forms.Button();
            this.dateTimePicker_general_inicio = new System.Windows.Forms.DateTimePicker();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.comboBox_tipo_agregar = new System.Windows.Forms.ComboBox();
            this.label9 = new System.Windows.Forms.Label();
            this.comboBox_Empleado_agregar = new System.Windows.Forms.ComboBox();
            this.textBox_pago = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.dataGridView_registro_individual = new System.Windows.Forms.DataGridView();
            this.comboBox_tipo_cargar = new System.Windows.Forms.ComboBox();
            this.button3 = new System.Windows.Forms.Button();
            this.comboBox_nombre_cargar = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox1_cedula_pagos = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tabControl_menu.SuspendLayout();
            this.Usuarios.SuspendLayout();
            this.usuario.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Empleados)).BeginInit();
            this.tabPage2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_registro_general)).BeginInit();
            this.groupBox2.SuspendLayout();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_registro_individual)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl_menu
            // 
            this.tabControl_menu.Controls.Add(this.Usuarios);
            this.tabControl_menu.Controls.Add(this.tabPage2);
            this.tabControl_menu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_menu.Location = new System.Drawing.Point(0, 0);
            this.tabControl_menu.Name = "tabControl_menu";
            this.tabControl_menu.SelectedIndex = 0;
            this.tabControl_menu.Size = new System.Drawing.Size(833, 447);
            this.tabControl_menu.TabIndex = 2;
            this.tabControl_menu.Tag = "";
            // 
            // Usuarios
            // 
            this.Usuarios.Controls.Add(this.textBox_Nombre);
            this.Usuarios.Controls.Add(this.label3);
            this.Usuarios.Controls.Add(this.usuario);
            this.Usuarios.Controls.Add(this.groupBox1);
            this.Usuarios.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Usuarios.Location = new System.Drawing.Point(4, 22);
            this.Usuarios.Name = "Usuarios";
            this.Usuarios.Padding = new System.Windows.Forms.Padding(3);
            this.Usuarios.Size = new System.Drawing.Size(825, 421);
            this.Usuarios.TabIndex = 0;
            this.Usuarios.Text = "Empleados";
            this.Usuarios.UseVisualStyleBackColor = true;
            // 
            // textBox_Nombre
            // 
            this.textBox_Nombre.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_Nombre.Font = new System.Drawing.Font("Segoe UI Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_Nombre.Location = new System.Drawing.Point(640, 49);
            this.textBox_Nombre.Name = "textBox_Nombre";
            this.textBox_Nombre.Size = new System.Drawing.Size(161, 25);
            this.textBox_Nombre.TabIndex = 5;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(488, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 21);
            this.label3.TabIndex = 3;
            // 
            // usuario
            // 
            this.usuario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.usuario.Controls.Add(this.button_update);
            this.usuario.Controls.Add(this.textBox_salario);
            this.usuario.Controls.Add(this.label4);
            this.usuario.Controls.Add(this.textBox_profecion);
            this.usuario.Controls.Add(this.button_limpiar);
            this.usuario.Controls.Add(this.textBox_telefono);
            this.usuario.Controls.Add(this.textBox_cedula);
            this.usuario.Controls.Add(this.label6);
            this.usuario.Controls.Add(this.button_agregar);
            this.usuario.Controls.Add(this.label5);
            this.usuario.Controls.Add(this.label1);
            this.usuario.Controls.Add(this.label2);
            this.usuario.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.usuario.Location = new System.Drawing.Point(517, 6);
            this.usuario.Name = "usuario";
            this.usuario.Size = new System.Drawing.Size(302, 406);
            this.usuario.TabIndex = 18;
            this.usuario.TabStop = false;
            this.usuario.Text = "Nuevo Cliente";
            // 
            // button_update
            // 
            this.button_update.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_update.Location = new System.Drawing.Point(198, 348);
            this.button_update.Name = "button_update";
            this.button_update.Size = new System.Drawing.Size(87, 31);
            this.button_update.TabIndex = 16;
            this.button_update.Text = "Actualizar";
            this.button_update.UseVisualStyleBackColor = true;
            this.button_update.Visible = false;
            this.button_update.Click += new System.EventHandler(this.button_update_Click);
            // 
            // textBox_salario
            // 
            this.textBox_salario.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_salario.Font = new System.Drawing.Font("Segoe UI Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_salario.Location = new System.Drawing.Point(123, 247);
            this.textBox_salario.Name = "textBox_salario";
            this.textBox_salario.Size = new System.Drawing.Size(161, 25);
            this.textBox_salario.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(23, 251);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 21);
            this.label4.TabIndex = 14;
            this.label4.Text = "Salario : ";
            // 
            // textBox_profecion
            // 
            this.textBox_profecion.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_profecion.Font = new System.Drawing.Font("Segoe UI Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_profecion.Location = new System.Drawing.Point(123, 194);
            this.textBox_profecion.Name = "textBox_profecion";
            this.textBox_profecion.Size = new System.Drawing.Size(161, 25);
            this.textBox_profecion.TabIndex = 13;
            // 
            // button_limpiar
            // 
            this.button_limpiar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_limpiar.Location = new System.Drawing.Point(16, 348);
            this.button_limpiar.Name = "button_limpiar";
            this.button_limpiar.Size = new System.Drawing.Size(82, 31);
            this.button_limpiar.TabIndex = 10;
            this.button_limpiar.Text = "Limpiar";
            this.button_limpiar.UseVisualStyleBackColor = true;
            this.button_limpiar.Click += new System.EventHandler(this.button_limpiar_Click);
            // 
            // textBox_telefono
            // 
            this.textBox_telefono.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_telefono.Font = new System.Drawing.Font("Segoe UI Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_telefono.Location = new System.Drawing.Point(123, 142);
            this.textBox_telefono.Name = "textBox_telefono";
            this.textBox_telefono.Size = new System.Drawing.Size(161, 25);
            this.textBox_telefono.TabIndex = 6;
            // 
            // textBox_cedula
            // 
            this.textBox_cedula.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_cedula.Font = new System.Drawing.Font("Segoe UI Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_cedula.Location = new System.Drawing.Point(123, 90);
            this.textBox_cedula.Name = "textBox_cedula";
            this.textBox_cedula.Size = new System.Drawing.Size(161, 25);
            this.textBox_cedula.TabIndex = 7;
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(23, 198);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(68, 21);
            this.label6.TabIndex = 12;
            this.label6.Text = "Puesto : ";
            // 
            // button_agregar
            // 
            this.button_agregar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_agregar.Location = new System.Drawing.Point(198, 348);
            this.button_agregar.Name = "button_agregar";
            this.button_agregar.Size = new System.Drawing.Size(75, 31);
            this.button_agregar.TabIndex = 9;
            this.button_agregar.Text = "Guardar";
            this.button_agregar.UseVisualStyleBackColor = true;
            this.button_agregar.Click += new System.EventHandler(this.button_agregar_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(22, 94);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 21);
            this.label5.TabIndex = 11;
            this.label5.Text = "Cedula : ";
            // 
            // label1
            // 
            this.label1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(21, 47);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(77, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Nombre : ";
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Light", 11F);
            this.label2.Location = new System.Drawing.Point(21, 147);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(75, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Telefono : ";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.button_editar);
            this.groupBox1.Controls.Add(this.button_eliminR);
            this.groupBox1.Controls.Add(this.dataGridView_Empleados);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.groupBox1.Location = new System.Drawing.Point(6, 6);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(505, 403);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Listado de Clientes";
            // 
            // button_editar
            // 
            this.button_editar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button_editar.Location = new System.Drawing.Point(384, 348);
            this.button_editar.Name = "button_editar";
            this.button_editar.Size = new System.Drawing.Size(80, 31);
            this.button_editar.TabIndex = 16;
            this.button_editar.Text = "Editar";
            this.button_editar.UseVisualStyleBackColor = true;
            this.button_editar.Click += new System.EventHandler(this.button_editar_Click);
            // 
            // button_eliminR
            // 
            this.button_eliminR.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button_eliminR.Location = new System.Drawing.Point(23, 348);
            this.button_eliminR.Name = "button_eliminR";
            this.button_eliminR.Size = new System.Drawing.Size(82, 31);
            this.button_eliminR.TabIndex = 15;
            this.button_eliminR.Text = "Eliminar";
            this.button_eliminR.UseVisualStyleBackColor = true;
            this.button_eliminR.Click += new System.EventHandler(this.button_eliminR_Click);
            // 
            // dataGridView_Empleados
            // 
            this.dataGridView_Empleados.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_Empleados.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Empleados.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Empleados.Location = new System.Drawing.Point(13, 47);
            this.dataGridView_Empleados.Name = "dataGridView_Empleados";
            this.dataGridView_Empleados.Size = new System.Drawing.Size(469, 293);
            this.dataGridView_Empleados.TabIndex = 0;
            this.dataGridView_Empleados.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_Empleados_CellMouseClick);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tabControl1);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(825, 421);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Pagos y vales";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(3, 3);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(819, 415);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.comboBox_filtrar);
            this.tabPage1.Controls.Add(this.dataGridView_registro_general);
            this.tabPage1.Controls.Add(this.dateTimePicker_general_final);
            this.tabPage1.Controls.Add(this.button4);
            this.tabPage1.Controls.Add(this.dateTimePicker_general_inicio);
            this.tabPage1.Controls.Add(this.groupBox2);
            this.tabPage1.Controls.Add(this.groupBox4);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(811, 389);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Agregar Pago ";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // comboBox_filtrar
            // 
            this.comboBox_filtrar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_filtrar.FormattingEnabled = true;
            this.comboBox_filtrar.Location = new System.Drawing.Point(37, 73);
            this.comboBox_filtrar.Name = "comboBox_filtrar";
            this.comboBox_filtrar.Size = new System.Drawing.Size(173, 25);
            this.comboBox_filtrar.TabIndex = 40;
            // 
            // dataGridView_registro_general
            // 
            this.dataGridView_registro_general.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_registro_general.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_registro_general.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_registro_general.Location = new System.Drawing.Point(21, 120);
            this.dataGridView_registro_general.Name = "dataGridView_registro_general";
            this.dataGridView_registro_general.Size = new System.Drawing.Size(455, 248);
            this.dataGridView_registro_general.TabIndex = 38;
            // 
            // dateTimePicker_general_final
            // 
            this.dateTimePicker_general_final.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker_general_final.Location = new System.Drawing.Point(252, 30);
            this.dateTimePicker_general_final.Name = "dateTimePicker_general_final";
            this.dateTimePicker_general_final.Size = new System.Drawing.Size(173, 20);
            this.dateTimePicker_general_final.TabIndex = 37;
            // 
            // button4
            // 
            this.button4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button4.Location = new System.Drawing.Point(281, 70);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(86, 31);
            this.button4.TabIndex = 36;
            this.button4.Text = "Cargar ";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.button4_Click);
            // 
            // dateTimePicker_general_inicio
            // 
            this.dateTimePicker_general_inicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimePicker_general_inicio.Location = new System.Drawing.Point(37, 30);
            this.dateTimePicker_general_inicio.Name = "dateTimePicker_general_inicio";
            this.dateTimePicker_general_inicio.Size = new System.Drawing.Size(173, 20);
            this.dateTimePicker_general_inicio.TabIndex = 35;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.textBox1_cedula_pagos);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.comboBox_tipo_agregar);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.comboBox_Empleado_agregar);
            this.groupBox2.Controls.Add(this.textBox_pago);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.groupBox2.Location = new System.Drawing.Point(503, 6);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(302, 362);
            this.groupBox2.TabIndex = 22;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Nuevo Pago";
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button2.Location = new System.Drawing.Point(192, 285);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(76, 31);
            this.button2.TabIndex = 28;
            this.button2.Text = "Agregar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // comboBox_tipo_agregar
            // 
            this.comboBox_tipo_agregar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_tipo_agregar.FormattingEnabled = true;
            this.comboBox_tipo_agregar.Location = new System.Drawing.Point(107, 167);
            this.comboBox_tipo_agregar.Name = "comboBox_tipo_agregar";
            this.comboBox_tipo_agregar.Size = new System.Drawing.Size(161, 25);
            this.comboBox_tipo_agregar.TabIndex = 27;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(34, 167);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(55, 21);
            this.label9.TabIndex = 26;
            this.label9.Text = "Tipo  : ";
            // 
            // comboBox_Empleado_agregar
            // 
            this.comboBox_Empleado_agregar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_Empleado_agregar.FormattingEnabled = true;
            this.comboBox_Empleado_agregar.Location = new System.Drawing.Point(107, 57);
            this.comboBox_Empleado_agregar.Name = "comboBox_Empleado_agregar";
            this.comboBox_Empleado_agregar.Size = new System.Drawing.Size(161, 25);
            this.comboBox_Empleado_agregar.TabIndex = 25;
            // 
            // textBox_pago
            // 
            this.textBox_pago.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox_pago.Font = new System.Drawing.Font("Segoe UI Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox_pago.Location = new System.Drawing.Point(107, 222);
            this.textBox_pago.Name = "textBox_pago";
            this.textBox_pago.Size = new System.Drawing.Size(161, 25);
            this.textBox_pago.TabIndex = 24;
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(35, 222);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(66, 21);
            this.label7.TabIndex = 23;
            this.label7.Text = "Salario : ";
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(35, 57);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(77, 21);
            this.label8.TabIndex = 22;
            this.label8.Text = "Nombre : ";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.groupBox4.Location = new System.Drawing.Point(6, 6);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(491, 368);
            this.groupBox4.TabIndex = 41;
            this.groupBox4.TabStop = false;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.dataGridView_registro_individual);
            this.tabPage4.Controls.Add(this.comboBox_tipo_cargar);
            this.tabPage4.Controls.Add(this.button3);
            this.tabPage4.Controls.Add(this.comboBox_nombre_cargar);
            this.tabPage4.Controls.Add(this.label14);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(811, 389);
            this.tabPage4.TabIndex = 2;
            this.tabPage4.Text = "Datos de pago de Empleado";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // dataGridView_registro_individual
            // 
            this.dataGridView_registro_individual.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_registro_individual.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_registro_individual.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_registro_individual.Location = new System.Drawing.Point(27, 91);
            this.dataGridView_registro_individual.Name = "dataGridView_registro_individual";
            this.dataGridView_registro_individual.Size = new System.Drawing.Size(757, 246);
            this.dataGridView_registro_individual.TabIndex = 33;
            // 
            // comboBox_tipo_cargar
            // 
            this.comboBox_tipo_cargar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_tipo_cargar.FormattingEnabled = true;
            this.comboBox_tipo_cargar.Location = new System.Drawing.Point(293, 43);
            this.comboBox_tipo_cargar.Name = "comboBox_tipo_cargar";
            this.comboBox_tipo_cargar.Size = new System.Drawing.Size(161, 25);
            this.comboBox_tipo_cargar.TabIndex = 32;
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(480, 40);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(86, 31);
            this.button3.TabIndex = 29;
            this.button3.Text = "Cargar ";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // comboBox_nombre_cargar
            // 
            this.comboBox_nombre_cargar.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboBox_nombre_cargar.FormattingEnabled = true;
            this.comboBox_nombre_cargar.Location = new System.Drawing.Point(109, 43);
            this.comboBox_nombre_cargar.Name = "comboBox_nombre_cargar";
            this.comboBox_nombre_cargar.Size = new System.Drawing.Size(161, 25);
            this.comboBox_nombre_cargar.TabIndex = 20;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(37, 43);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(77, 21);
            this.label14.TabIndex = 19;
            this.label14.Text = "Nombre : ";
            // 
            // textBox1_cedula_pagos
            // 
            this.textBox1_cedula_pagos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1_cedula_pagos.Font = new System.Drawing.Font("Segoe UI Light", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1_cedula_pagos.Location = new System.Drawing.Point(107, 114);
            this.textBox1_cedula_pagos.Name = "textBox1_cedula_pagos";
            this.textBox1_cedula_pagos.Size = new System.Drawing.Size(161, 25);
            this.textBox1_cedula_pagos.TabIndex = 29;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(34, 118);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(73, 21);
            this.label10.TabIndex = 30;
            this.label10.Text = "Cedula  : ";
            // 
            // formulario_empleados
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(833, 447);
            this.Controls.Add(this.tabControl_menu);
            this.Name = "formulario_empleados";
            this.Text = "formulario_empleados";
            this.tabControl_menu.ResumeLayout(false);
            this.Usuarios.ResumeLayout(false);
            this.Usuarios.PerformLayout();
            this.usuario.ResumeLayout(false);
            this.usuario.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Empleados)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_registro_general)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.tabPage4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_registro_individual)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl_menu;
        private System.Windows.Forms.TabPage Usuarios;
        private System.Windows.Forms.TextBox textBox_profecion;
        private System.Windows.Forms.TextBox textBox_cedula;
        private System.Windows.Forms.TextBox textBox_telefono;
        private System.Windows.Forms.TextBox textBox_Nombre;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DataGridView dataGridView_Empleados;
        private System.Windows.Forms.GroupBox usuario;
        private System.Windows.Forms.Button button_limpiar;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button button_agregar;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TextBox textBox_salario;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ComboBox comboBox_nombre_cargar;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.DataGridView dataGridView_registro_individual;
        private System.Windows.Forms.ComboBox comboBox_tipo_cargar;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox comboBox_tipo_agregar;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.ComboBox comboBox_Empleado_agregar;
        private System.Windows.Forms.TextBox textBox_pago;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboBox_filtrar;
        private System.Windows.Forms.DataGridView dataGridView_registro_general;
        private System.Windows.Forms.DateTimePicker dateTimePicker_general_final;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.DateTimePicker dateTimePicker_general_inicio;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button button_eliminR;
        private System.Windows.Forms.Button button_editar;
        private System.Windows.Forms.Button button_update;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox textBox1_cedula_pagos;
    }
}