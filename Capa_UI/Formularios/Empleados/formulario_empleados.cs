﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using principal.Empleados;

namespace Capa_UI.Formularios.Empleados
{

    public partial class formulario_empleados : Form
    {
        logica_Empleados Modelo_empleado = new logica_Empleados();
        Logica_pagos_y_vales objeto = new Logica_pagos_y_vales();
        public formulario_empleados()
        {
            
            InitializeComponent();
            Cargar_datos_Empleados();
            llenar_combobox_pagos_y_vales();
        }
        public void Cargar_datos_Empleados()
        {
            dataGridView_Empleados.DataSource = Modelo_empleado.mostrar("select * From Empleados");
        }
        public void limpio()
        {
            textBox_Nombre.Text = "";
            textBox_cedula.Text = "";
            textBox_telefono.Text = "";
            textBox_profecion.Text = "";
            textBox_salario.Text = "";

        }
        private void button_limpiar_Click(object sender, EventArgs e)
        {
            limpio();
        }

        private void button_agregar_Click(object sender, EventArgs e)
        {
            string mensage = Modelo_empleado.insertar(textBox_Nombre.Text, textBox_cedula.Text, textBox_telefono.Text, textBox_profecion.Text, textBox_salario.Text);
            MessageBox.Show(mensage);
            Cargar_datos_Empleados();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }

        //pestaña pagos y vales
        public void llenar_combobox_pagos_y_vales()
        {
            List<string> lista = new List<string>();
            List<string> lista2 = new List<string>();
            lista2.Add("Pago");
            lista2.Add("Vale");
            lista = objeto.LLenar_combox("select Nombre from Empleados");
            comboBox_Empleado_agregar.DataSource = lista;
            comboBox_nombre_cargar.DataSource = lista;
            comboBox_filtrar.DataSource = lista2;
            comboBox_tipo_agregar.DataSource = lista2;
            comboBox_tipo_cargar.DataSource = lista2;

        }
        public void Cargar_Registros_De_pagos()
        {
            var fechainicio = Convert.ToString(dateTimePicker_general_inicio.Value.AddDays(-1));
            var fechafinal = Convert.ToString(dateTimePicker_general_final.Value);
            dataGridView_registro_general.DataSource = objeto.Mostrar_registros(fechainicio,fechafinal,comboBox_filtrar.Text);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            string mensage = objeto.insertar(comboBox_Empleado_agregar.Text, comboBox_tipo_agregar.Text, DateTime.Today.ToString(), textBox_pago.Text,textBox1_cedula_pagos.Text);
            MessageBox.Show(mensage);

        }

        private void button4_Click(object sender, EventArgs e)
        {
            Cargar_Registros_De_pagos();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            dataGridView_registro_individual.DataSource = objeto.registros_indiviuales(comboBox_nombre_cargar.Text, comboBox_tipo_cargar.Text);
        }
        string id_empleado = "";
        string Nombre = "";
        string Cedula = "";
        string Telefono = "";
        string Cargo= "";
        string salario = "";
        private void dataGridView_Empleados_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewRow fila = dataGridView_Empleados.Rows[e.RowIndex];
            id_empleado= Convert.ToString(fila.Cells[0].Value);
            Nombre = Convert.ToString(fila.Cells[1].Value);
            Cedula= Convert.ToString(fila.Cells[2].Value);
            Telefono = Convert.ToString(fila.Cells[3].Value);
            Cargo = Convert.ToString(fila.Cells[4].Value);
            salario = Convert.ToString(fila.Cells[5].Value);
        }

        private void button_eliminR_Click(object sender, EventArgs e)
        {
            if (id_empleado != "")
            {
                string mensage = Modelo_empleado.delete(string.Format("delete from Empleados where id_empleado = {0}", id_empleado));
                MessageBox.Show(mensage);
                id_empleado = "";
                Cargar_datos_Empleados();
            }
            else
            {
                MessageBox.Show("no hay empleado seleccionado");
            }
        }

        private void button_editar_Click(object sender, EventArgs e)
        {

            if (id_empleado != "")
            {
                button_update.Visible = true;
                button_agregar.Visible = false;
                textBox_Nombre.Text = Nombre;
                textBox_cedula.Text = Cedula;
                textBox_telefono.Text = Telefono;
                textBox_profecion.Text = Cargo;
                textBox_salario.Text = salario;
            }
            else
            {
                MessageBox.Show("no hay empleado seleccionado");
            }
           
        }

        private void button_update_Click(object sender, EventArgs e)
        {
            string mensage = Modelo_empleado.update(string.Format("update Empleados set Nombre ='{0}',cedula= '{1}',Telefono = '{2}',cargo = '{3}',sueldo = '{4}' where id_empleado = {5}", new string[] {textBox_Nombre.Text,textBox_cedula.Text,textBox_telefono.Text,textBox_profecion.Text,textBox_salario.Text,id_empleado }));
            string mensage2 = Modelo_empleado.update(string.Format("update TablaPagos_y_Vales set Nombre ='{0}',cedula= '{1}' where cedula = {2}", new string[] { textBox_Nombre.Text, textBox_cedula.Text,textBox_cedula.Text }));
            MessageBox.Show(mensage);
            id_empleado = "";
            button_update.Visible = false;
            button_agregar.Visible = true;
            limpio();
            Cargar_datos_Empleados();

        }
    }
}
