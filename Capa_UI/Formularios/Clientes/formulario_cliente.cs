﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using principal.clientes;
namespace Capa_UI.Formularios.Clientes
{
    public partial class formulario_cliente : Form
    {
        logica_clientes clienteModelo = new logica_clientes();
        public formulario_cliente()
        {
            InitializeComponent();
            cargar_datos_de_clienetes();
            cargar_cedulas();
        }


        public void cargar_datos_de_clienetes()
        {
            dataGridView_clientes.DataSource = clienteModelo.mostrar("select Id_cliente as ID,Nombre, Apellido,Cedula,Telefono,Cargo as Profecion from clientes");
        }

        private void button_agregar_Click(object sender, EventArgs e)
        {
            string mensage = clienteModelo.insertar(textBox_Nombre.Text, textBox_apellido.Text, textBox_cedula.Text, textBox_telefono.Text, textBox_profecion.Text);
            MessageBox.Show(mensage);
            cargar_datos_de_clienetes();

        }
        public void limpio()
        {
            textBox_Nombre.Text = null;
            textBox_apellido.Text = null;
            textBox_cedula.Text = null;
            textBox_telefono.Text = null;
            textBox_profecion.Text = null;
        }
        private void button2_Click(object sender, EventArgs e)
        {
            limpio();
        }

        public void cargar_cedulas()
        {
            comboBox_cedula.DataSource = clienteModelo.mostrar_lista("select cedula from clientes order by cedula desc");
        }

        private void button_gargar_Click(object sender, EventArgs e)
        {
            string tabla = "select codigo_de_venta,Nombre_cliente,Referencia,Fecha ,sum(Monto) as Monto ,Tipo_de_pago as [Tipo de Pago] " +
                         "from Tabla_Ventas where cedula ='"+comboBox_cedula.Text+"'"+
                         "group by codigo_de_venta,Referencia, Nombre_cliente,Fecha,Tipo_de_pago order by Fecha desc";
            dataGridView1.DataSource = clienteModelo.mostrar(tabla);
        }
        string id_cliente= "";
        string Nombre= "";
        string Apellido = "";
        string Cedula= "";
        string Telefono= "";
        string profecion= "";
        private void dataGridView_clientes_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewRow fila = dataGridView_clientes.Rows[e.RowIndex];
            id_cliente= Convert.ToString(fila.Cells[0].Value);
            Nombre  = Convert.ToString(fila.Cells[1].Value);
            Apellido = Convert.ToString(fila.Cells[2].Value);
            Cedula  = Convert.ToString(fila.Cells[3].Value);
            Telefono = Convert.ToString(fila.Cells[4].Value);
            profecion = Convert.ToString(fila.Cells[5].Value);
        }

        private void button_eliminR_Click(object sender, EventArgs e)
        {
            if (id_cliente != "")
            {
                string mensage = clienteModelo.delete(string.Format("delete from cliente where id_cliente = {0}", id_cliente));
                MessageBox.Show(mensage);
                id_cliente = "";
                cargar_datos_de_clienetes();
            }
            else
            {
                MessageBox.Show("no hay cliente seleccionado");
            }
        }

        private void button_editar_Click(object sender, EventArgs e)
        {
            if (id_cliente!= "")
            {
                button_update.Visible = true;
                button_agregar.Visible = false;
                textBox_Nombre.Text = Nombre;
                textBox_apellido.Text = Apellido;
                textBox_cedula.Text = Cedula;
                textBox_telefono.Text = Telefono;
                textBox_profecion.Text = profecion;

            }
            else
            {
                MessageBox.Show("no hay cliente seleccionada");
            }
        }
       
        private void button_update_Click(object sender, EventArgs e)
        {
            string mensage = clienteModelo.update(string.Format("update clientes set Nombre ='{0}',Apellido = '{1}',cedula= '{2}',Telefono = '{3}',cargo = '{4}' where id_cliente= {5}", new string[] { textBox_Nombre.Text,textBox_apellido.Text,textBox_cedula.Text,textBox_telefono.Text,textBox_profecion.Text, id_cliente }));
            string query = "update Tabla_Ventas set Nombre_cliente = '" + textBox_Nombre.Text + " " + textBox_apellido.Text + "',cedula = '" + textBox_cedula.Text + "' where cedula = '" + textBox_cedula.Text + "'";
            string mensage2 = clienteModelo.update(query);
            MessageBox.Show(mensage);
           
            id_cliente = "";
            button_update.Visible = false;
            button_agregar.Visible = true;
            limpio();
            cargar_datos_de_clienetes();
        }
    }
}
