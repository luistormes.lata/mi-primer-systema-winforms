﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using principal.sistema;
namespace Capa_UI.Formularios.sistema
{

    public partial class sistema : Form
    {
        logica_sistema objeto = new logica_sistema();
        public sistema()
        {
            InitializeComponent();
            cargar_datos();

        }
        public void cargar_datos()
        {
            dataGridView_usuarios.DataSource = objeto.mostrar("select * from TablaUsuarios");
        }
        private void button_agregar_Click(object sender, EventArgs e)
        {
            string mensage = objeto.insertar_Usuarios(textBox_Nombre.Text, textBox_usuario.Text, textBox_contraceña.Text, textBox_repetir_contraceña.Text, textBox_correo.Text);
            MessageBox.Show(mensage);
            cargar_datos();

        }
        public void limpio()
        {
            textBox_Nombre.Text = "";
            textBox_usuario.Text = "";
            textBox_contraceña.Text = "";
            textBox_repetir_contraceña.Text = "";
            textBox_correo.Text = "";
        }
        private void button2_Click(object sender, EventArgs e)
        {

            limpio();
        }

        #region nulo
        private void Usuarios_Click(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }
   

        private void label4_Click(object sender, EventArgs e)
        {

        }

      

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
        #endregion

       

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }

        private void dataGridView_usuarios_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        string id_usuario = "";
        string nombre = "";
        string usuario_ = "";
        string contraceña_usuario = "";
        string correo_usuario = "";
        private void dataGridView_usuarios_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewRow fila = dataGridView_usuarios.Rows[e.RowIndex];
            id_usuario = Convert.ToString(fila.Cells[0].Value);
            nombre = Convert.ToString(fila.Cells[1].Value);
            usuario_ = Convert.ToString(fila.Cells[2].Value);
            contraceña_usuario = Convert.ToString(fila.Cells[3].Value);
            correo_usuario = Convert.ToString(fila.Cells[4].Value);


        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(id_usuario != "")
            {
                string mensage = objeto.delete(string.Format("delete from TablaUsuarios where id_usuario = {0} and id_usuario != 2 ",id_usuario));
                MessageBox.Show(mensage);
                id_usuario = "";
                cargar_datos();
            }
            else
            {
                MessageBox.Show("no hay usuario seleccionada");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (id_usuario != "")
            {
                button_update.Visible = true;
                button_agregar.Visible = false;
                textBox_Nombre.Text = nombre;
                textBox_usuario.Text = usuario_;
                textBox_contraceña.Text = contraceña_usuario;
                textBox_correo.Text = correo_usuario;
                
            }
            else
            {
                MessageBox.Show("no hay usuario seleccionada");
            }
        }

        private void button_update_Click(object sender, EventArgs e)
        {
            if (textBox_contraceña.Text == textBox_repetir_contraceña.Text)
            {
                string mensage = objeto.update(string.Format("update TablaUsuarios set nombre ='{0}', usuario= '{1}',passwords = '{2}',Email = '{3}' where id_usuario = {4}", new string[] { textBox_Nombre.Text, textBox_usuario.Text, textBox_contraceña.Text, textBox_correo.Text, id_usuario }));
                MessageBox.Show(mensage);
                id_usuario = "";
                button_update.Visible = false;
                button_agregar.Visible = true;
                limpio();
                cargar_datos();
            }
            else
            {
                MessageBox.Show("las contraceñas no coinciden");
            }
        }
    }
}
