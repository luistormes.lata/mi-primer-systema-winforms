﻿namespace Capa_UI.Formularios.Gastos
{
    partial class Formulario_gastos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabControl_menu = new System.Windows.Forms.TabControl();
            this.Usuarios = new System.Windows.Forms.TabPage();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.dataGridView_gastos = new System.Windows.Forms.DataGridView();
            this.label_Montotal = new System.Windows.Forms.Label();
            this.button_limpiar = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.usuario = new System.Windows.Forms.GroupBox();
            this.textBox_descrip = new System.Windows.Forms.TextBox();
            this.textBox_monto = new System.Windows.Forms.TextBox();
            this.button_agregar = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.comboBox_motivo = new System.Windows.Forms.ComboBox();
            this.comboBox_tipo = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView_semana = new System.Windows.Forms.DataGridView();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.dataGridView_Mes = new System.Windows.Forms.DataGridView();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.button_cargar = new System.Windows.Forms.Button();
            this.dateTime_final = new System.Windows.Forms.DateTimePicker();
            this.dateTimeinicio = new System.Windows.Forms.DateTimePicker();
            this.dataGridView_busqueda = new System.Windows.Forms.DataGridView();
            this.tabControl_menu.SuspendLayout();
            this.Usuarios.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_gastos)).BeginInit();
            this.usuario.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_semana)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Mes)).BeginInit();
            this.tabPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_busqueda)).BeginInit();
            this.SuspendLayout();
            // 
            // tabControl_menu
            // 
            this.tabControl_menu.Controls.Add(this.Usuarios);
            this.tabControl_menu.Controls.Add(this.tabPage2);
            this.tabControl_menu.Controls.Add(this.tabPage3);
            this.tabControl_menu.Controls.Add(this.tabPage4);
            this.tabControl_menu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_menu.Location = new System.Drawing.Point(0, 0);
            this.tabControl_menu.Name = "tabControl_menu";
            this.tabControl_menu.SelectedIndex = 0;
            this.tabControl_menu.Size = new System.Drawing.Size(800, 450);
            this.tabControl_menu.TabIndex = 3;
            this.tabControl_menu.Tag = "";
            // 
            // Usuarios
            // 
            this.Usuarios.Controls.Add(this.groupBox1);
            this.Usuarios.Controls.Add(this.label3);
            this.Usuarios.Controls.Add(this.usuario);
            this.Usuarios.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Usuarios.Location = new System.Drawing.Point(4, 22);
            this.Usuarios.Name = "Usuarios";
            this.Usuarios.Padding = new System.Windows.Forms.Padding(3);
            this.Usuarios.Size = new System.Drawing.Size(792, 424);
            this.Usuarios.TabIndex = 0;
            this.Usuarios.Text = "Gastos";
            this.Usuarios.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.dataGridView_gastos);
            this.groupBox1.Controls.Add(this.label_Montotal);
            this.groupBox1.Controls.Add(this.button_limpiar);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.groupBox1.Location = new System.Drawing.Point(8, 156);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(776, 260);
            this.groupBox1.TabIndex = 21;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Gastos de hoy";
            this.groupBox1.Enter += new System.EventHandler(this.groupBox1_Enter);
            // 
            // dataGridView_gastos
            // 
            this.dataGridView_gastos.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_gastos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_gastos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_gastos.Location = new System.Drawing.Point(22, 45);
            this.dataGridView_gastos.Name = "dataGridView_gastos";
            this.dataGridView_gastos.Size = new System.Drawing.Size(733, 152);
            this.dataGridView_gastos.TabIndex = 23;
            // 
            // label_Montotal
            // 
            this.label_Montotal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label_Montotal.AutoSize = true;
            this.label_Montotal.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label_Montotal.Location = new System.Drawing.Point(624, 232);
            this.label_Montotal.Name = "label_Montotal";
            this.label_Montotal.Size = new System.Drawing.Size(18, 21);
            this.label_Montotal.TabIndex = 22;
            this.label_Montotal.Text = "0";
            // 
            // button_limpiar
            // 
            this.button_limpiar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.button_limpiar.Location = new System.Drawing.Point(22, 224);
            this.button_limpiar.Name = "button_limpiar";
            this.button_limpiar.Size = new System.Drawing.Size(90, 31);
            this.button_limpiar.TabIndex = 10;
            this.button_limpiar.Text = "Limpiar";
            this.button_limpiar.UseVisualStyleBackColor = true;
            this.button_limpiar.Click += new System.EventHandler(this.button_limpiar_Click);
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(447, 229);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(171, 21);
            this.label7.TabIndex = 15;
            this.label7.Text = "Monto total  de Gastos: ";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(455, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 21);
            this.label3.TabIndex = 3;
            // 
            // usuario
            // 
            this.usuario.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.usuario.Controls.Add(this.textBox_descrip);
            this.usuario.Controls.Add(this.textBox_monto);
            this.usuario.Controls.Add(this.button_agregar);
            this.usuario.Controls.Add(this.label5);
            this.usuario.Controls.Add(this.comboBox_motivo);
            this.usuario.Controls.Add(this.comboBox_tipo);
            this.usuario.Controls.Add(this.label4);
            this.usuario.Controls.Add(this.label2);
            this.usuario.Controls.Add(this.label1);
            this.usuario.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.usuario.Location = new System.Drawing.Point(-4, 6);
            this.usuario.Name = "usuario";
            this.usuario.Size = new System.Drawing.Size(788, 144);
            this.usuario.TabIndex = 18;
            this.usuario.TabStop = false;
            this.usuario.Text = "Nuevo Gasto";
            // 
            // textBox_descrip
            // 
            this.textBox_descrip.Location = new System.Drawing.Point(439, 41);
            this.textBox_descrip.Name = "textBox_descrip";
            this.textBox_descrip.Size = new System.Drawing.Size(264, 29);
            this.textBox_descrip.TabIndex = 17;
            // 
            // textBox_monto
            // 
            this.textBox_monto.Location = new System.Drawing.Point(439, 87);
            this.textBox_monto.Name = "textBox_monto";
            this.textBox_monto.Size = new System.Drawing.Size(158, 29);
            this.textBox_monto.TabIndex = 16;
            this.textBox_monto.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // button_agregar
            // 
            this.button_agregar.Location = new System.Drawing.Point(630, 87);
            this.button_agregar.Name = "button_agregar";
            this.button_agregar.Size = new System.Drawing.Size(75, 31);
            this.button_agregar.TabIndex = 9;
            this.button_agregar.Text = "Guardar";
            this.button_agregar.UseVisualStyleBackColor = true;
            this.button_agregar.Click += new System.EventHandler(this.button_agregar_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(334, 95);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 21);
            this.label5.TabIndex = 15;
            this.label5.Text = "Monto  : ";
            // 
            // comboBox_motivo
            // 
            this.comboBox_motivo.FormattingEnabled = true;
            this.comboBox_motivo.Location = new System.Drawing.Point(135, 89);
            this.comboBox_motivo.Name = "comboBox_motivo";
            this.comboBox_motivo.Size = new System.Drawing.Size(158, 29);
            this.comboBox_motivo.TabIndex = 14;
            // 
            // comboBox_tipo
            // 
            this.comboBox_tipo.FormattingEnabled = true;
            this.comboBox_tipo.Location = new System.Drawing.Point(135, 46);
            this.comboBox_tipo.Name = "comboBox_tipo";
            this.comboBox_tipo.Size = new System.Drawing.Size(158, 29);
            this.comboBox_tipo.TabIndex = 13;
            this.comboBox_tipo.SelectedIndexChanged += new System.EventHandler(this.comboBox_tipo_SelectedIndexChanged);
            this.comboBox_tipo.TextUpdate += new System.EventHandler(this.comboBox_tipo_TextUpdate);
            this.comboBox_tipo.TextChanged += new System.EventHandler(this.comboBox_tipo_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(27, 49);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(109, 21);
            this.label4.TabIndex = 12;
            this.label4.Text = "Tipo de gasto: ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(27, 97);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(68, 21);
            this.label2.TabIndex = 11;
            this.label2.Text = "Motivo : ";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(334, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(99, 21);
            this.label1.TabIndex = 1;
            this.label1.Text = "Descripcion : ";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.dataGridView_semana);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(792, 424);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Gastos de la semana";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView_semana
            // 
            this.dataGridView_semana.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_semana.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_semana.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_semana.Location = new System.Drawing.Point(30, 61);
            this.dataGridView_semana.Name = "dataGridView_semana";
            this.dataGridView_semana.Size = new System.Drawing.Size(733, 316);
            this.dataGridView_semana.TabIndex = 24;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.dataGridView_Mes);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(792, 424);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Gastos del mes";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // dataGridView_Mes
            // 
            this.dataGridView_Mes.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_Mes.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_Mes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_Mes.Location = new System.Drawing.Point(30, 40);
            this.dataGridView_Mes.Name = "dataGridView_Mes";
            this.dataGridView_Mes.Size = new System.Drawing.Size(733, 342);
            this.dataGridView_Mes.TabIndex = 24;
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.button_cargar);
            this.tabPage4.Controls.Add(this.dateTime_final);
            this.tabPage4.Controls.Add(this.dateTimeinicio);
            this.tabPage4.Controls.Add(this.dataGridView_busqueda);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(792, 424);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Buscar por fecha";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // button_cargar
            // 
            this.button_cargar.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.button_cargar.Location = new System.Drawing.Point(504, 29);
            this.button_cargar.Name = "button_cargar";
            this.button_cargar.Size = new System.Drawing.Size(88, 29);
            this.button_cargar.TabIndex = 27;
            this.button_cargar.Text = "Cargar";
            this.button_cargar.UseVisualStyleBackColor = true;
            this.button_cargar.Click += new System.EventHandler(this.button_cargar_Click);
            // 
            // dateTime_final
            // 
            this.dateTime_final.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTime_final.Location = new System.Drawing.Point(273, 32);
            this.dateTime_final.Name = "dateTime_final";
            this.dateTime_final.Size = new System.Drawing.Size(211, 20);
            this.dateTime_final.TabIndex = 26;
            // 
            // dateTimeinicio
            // 
            this.dateTimeinicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimeinicio.Location = new System.Drawing.Point(35, 32);
            this.dateTimeinicio.Name = "dateTimeinicio";
            this.dateTimeinicio.Size = new System.Drawing.Size(211, 20);
            this.dateTimeinicio.TabIndex = 25;
            // 
            // dataGridView_busqueda
            // 
            this.dataGridView_busqueda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_busqueda.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_busqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_busqueda.Location = new System.Drawing.Point(30, 79);
            this.dataGridView_busqueda.Name = "dataGridView_busqueda";
            this.dataGridView_busqueda.Size = new System.Drawing.Size(733, 285);
            this.dataGridView_busqueda.TabIndex = 24;
            // 
            // Formulario_gastos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.tabControl_menu);
            this.Name = "Formulario_gastos";
            this.Text = "Formulario_gastos";
            this.tabControl_menu.ResumeLayout(false);
            this.Usuarios.ResumeLayout(false);
            this.Usuarios.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_gastos)).EndInit();
            this.usuario.ResumeLayout(false);
            this.usuario.PerformLayout();
            this.tabPage2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_semana)).EndInit();
            this.tabPage3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_Mes)).EndInit();
            this.tabPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_busqueda)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl_menu;
        private System.Windows.Forms.TabPage Usuarios;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox usuario;
        private System.Windows.Forms.Button button_limpiar;
        private System.Windows.Forms.Button button_agregar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox comboBox_tipo;
        private System.Windows.Forms.ComboBox comboBox_motivo;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox textBox_monto;
        private System.Windows.Forms.TextBox textBox_descrip;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label_Montotal;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.DataGridView dataGridView_gastos;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.DataGridView dataGridView_semana;
        private System.Windows.Forms.DataGridView dataGridView_Mes;
        private System.Windows.Forms.DataGridView dataGridView_busqueda;
        private System.Windows.Forms.Button button_cargar;
        private System.Windows.Forms.DateTimePicker dateTime_final;
        private System.Windows.Forms.DateTimePicker dateTimeinicio;
    }
}