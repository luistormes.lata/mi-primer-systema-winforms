﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using principal.GAstos;

namespace Capa_UI.Formularios.Gastos
{
    public partial class Formulario_gastos : Form
    {
        Logica_gastos objeto = new Logica_gastos();

        public Formulario_gastos()
        {
            InitializeComponent();
            cargar_datos_de_gastos();
            llenar_combobox_Tipo();
            Gastos_por_periodo();
        }
        List<string> total = new List<string>();
        public void cargar_datos_de_gastos()
        {
            dataGridView_gastos.DataSource = objeto.mostrar_por_periodo(DateTime.Today.ToString(), DateTime.Now.ToString());
            string query = "select sum(Monto) from Gastos";
            total= objeto.mostrar_lista(query);
            label_Montotal.Text = total[0];
        }
        private void button_agregar_Click(object sender, EventArgs e)
        {
            String fecha = DateTime.Now.ToString();
            string mensage = objeto.insertar(comboBox_tipo.Text, fecha, comboBox_motivo.Text, textBox_descrip.Text, textBox_monto.Text);
            MessageBox.Show(mensage);
            cargar_datos_de_gastos();
        }
        public void llenar_combobox_Tipo()
        {
            List<string> Tipo = new List<string>();
      
            Tipo.Add("Servicio");
            Tipo.Add("Inventario");
            Tipo.Add("Renta");

            comboBox_tipo.DataSource = Tipo;
        }
        public void llenarcombox_Motivo()
        {
            List<string> Motivo = new List<string>();

            if(comboBox_tipo.Text == "Servicio")
            {
                Motivo.Clear();
                Motivo.Add("Transporte");
                Motivo.Add("luz");
                Motivo.Add("Gas");
                Motivo.Add("Telefono");
                Motivo.Add("Internet");
                comboBox_motivo.DataSource = Motivo;
            }
            if (comboBox_tipo.Text == "Inventario")
            {
                Motivo.Clear();
                Motivo.Add("Biveres");
                Motivo.Add("Charcuteria");
                Motivo.Add("Agua");
                Motivo.Add("Electrodomesticos");
                Motivo.Add("Utencilios");
                comboBox_motivo.DataSource = Motivo;
            }
            if (comboBox_tipo.Text == "Renta")
            {
                Motivo.Clear();
                Motivo.Add("Biopago");
                Motivo.Add("Punto");
                Motivo.Add("Local");
                comboBox_motivo.DataSource = Motivo;
            }
        }
        #region nulo
        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        private void comboBox_tipo_TextChanged(object sender, EventArgs e)
        {
            llenarcombox_Motivo();
        }

        private void comboBox_tipo_TextUpdate(object sender, EventArgs e)
        {
            llenarcombox_Motivo();
        }

        #endregion

        private void comboBox_tipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            llenarcombox_Motivo();
        }

        private void button_limpiar_Click(object sender, EventArgs e)
        {
            comboBox_motivo.Text = "";
            comboBox_tipo.Text = "";
            textBox_monto.Text = "";
            textBox_descrip.Text = "";
        }
        public void Gastos_por_periodo()
        {
           
            // ultima semana

            var fechainicio = Convert.ToString(DateTime.Today.AddDays(-7));
            string fechafinal = DateTime.Now.ToString();
            dataGridView_semana.DataSource = objeto.mostrar_por_periodo(fechainicio, fechafinal);

            //ultimo mes

            fechainicio = Convert.ToString(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
            dataGridView_Mes.DataSource = objeto.mostrar_por_periodo(fechainicio, fechafinal);
        }
        
        private void button_cargar_Click(object sender, EventArgs e)
        {
            var fechainicio = Convert.ToString(dateTimeinicio.Value.AddDays(-1));
            var fechafinal = Convert.ToString(dateTime_final.Value);
            dataGridView_busqueda.DataSource = objeto.mostrar_por_periodo(fechainicio, fechafinal);
        }
    }
}
