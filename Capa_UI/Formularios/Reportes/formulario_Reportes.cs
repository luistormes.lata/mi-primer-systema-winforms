﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using principal.reportes;
namespace Capa_UI.Formularios.Reportes
{
    public partial class formulario_Reportes : Form
    {
        logica_reportes objeto = new logica_reportes();
        public formulario_Reportes()
        {
            InitializeComponent();
            cargar_datos_Registro();
            Reporte_Hoy();
            
        }
        public void cargar_datos_Registro()
        {
            List<string> lista_totales = new List<string>();
            lista_totales = objeto.Reporte_Registros();
            label_P.Text = lista_totales[0];
            label_C.Text = lista_totales[1];
            label_U.Text = lista_totales[2];
            label_E.Text = lista_totales[3];
           
        }
        public void productos_vendidos_por_periodo(string fecha_ini,string feha_final)
        {
            dataGridView1.DataSource = objeto.mostrar("select Productos as [Productos Vendidos ],sum(Cant_Productos) as Cantidad from Tabla_Ventas where fecha between '"+fecha_ini+"' and '"+feha_final+"' group by  Productos");
        }
        // reportes de totales
        public void reporte_de_ingresos(string fechainicio,string fechafinal)
        {
           string query = "select sum (Monto) as [Ingresos Totales] from Tabla_Ventas where fecha between '" + fechainicio + "' and '" + fechafinal + "'";
            dataGridView2.DataSource = objeto.mostrar(query);

        }
        public void reporte_de_gastos(string fechainicio, string fechafinal)
        {
            string query = "select sum (Monto) as [Gastos Totales] from Gastos where fecha between '" + fechainicio + "' and '" + fechafinal + "'";
            dataGridView3.DataSource = objeto.mostrar(query);

        }
        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }
        public void Reporte_Hoy()
        {
            reporte_de_gastos(DateTime.Today.ToString(), DateTime.Now.ToString());
            reporte_de_ingresos(DateTime.Today.ToString(), DateTime.Now.ToString());
            productos_vendidos_por_periodo(DateTime.Today.ToString(),DateTime.Now.ToString());
        }
       
        private void formulario_Reportes_Load(object sender, EventArgs e)
        {
            
           
        }

        private void groupBox2_Enter(object sender, EventArgs e)
        {

        }

        private void button_cargar_Click(object sender, EventArgs e)
        {
          
            string fechainicio = Convert.ToString(dateTimeinicio.Value.ToString());
            string fechafinal = Convert.ToString(dateTime_final.Value.ToString());
            string query = "select Tabla_Ventas.Fecha,sum(Tabla_Ventas.Monto) as ingreso, sum(Gastos.Monto) as Gastos from Tabla_Ventas,Gastos"+
                            "where Tabla_Ventas.Fecha between '"+fechainicio+"' and '"+fechafinal+"' group by Tabla_Ventas.Fecha ";
            dataGridView_busqueda.DataSource = objeto.mostrar(query);
        }

        private void tagpage1_Click(object sender, EventArgs e)
        {
            //  string query = "select Fecha,sum(Monto) as [Ingresos Totales] from Tabla_Ventas"
            //+"where Fecha between '" + fechainicio + "' and '" + fechafinal + "' group by Fecha order by Fecha desc";
            //dataGridView_busqueda_ingreso.DataSource = objeto.mostrar(query);*//
        }
    }
}
