﻿namespace Capa_UI.Formularios.ventas.Detalles_de_la_venta
{
    partial class Detalles_venta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dataGridView_productos = new System.Windows.Forms.DataGridView();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label_monto_total = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.CodigoV = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label_name_cliente = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label_referencia = new System.Windows.Forms.Label();
            this.label_tipo_de_pago = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_productos)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // dataGridView_productos
            // 
            this.dataGridView_productos.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_productos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_productos.Location = new System.Drawing.Point(12, 184);
            this.dataGridView_productos.Name = "dataGridView_productos";
            this.dataGridView_productos.Size = new System.Drawing.Size(404, 189);
            this.dataGridView_productos.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.label_monto_total);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.groupBox1.Location = new System.Drawing.Point(-3, 155);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(445, 256);
            this.groupBox1.TabIndex = 20;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Productos";
            // 
            // label_monto_total
            // 
            this.label_monto_total.AutoSize = true;
            this.label_monto_total.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label_monto_total.Location = new System.Drawing.Point(342, 228);
            this.label_monto_total.Name = "label_monto_total";
            this.label_monto_total.Size = new System.Drawing.Size(18, 21);
            this.label_monto_total.TabIndex = 30;
            this.label_monto_total.Text = "0";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label7.Location = new System.Drawing.Point(270, 228);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 21);
            this.label7.TabIndex = 29;
            this.label7.Text = "Total  : ";
            // 
            // CodigoV
            // 
            this.CodigoV.AutoSize = true;
            this.CodigoV.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.CodigoV.Location = new System.Drawing.Point(326, 24);
            this.CodigoV.Name = "CodigoV";
            this.CodigoV.Size = new System.Drawing.Size(18, 21);
            this.CodigoV.TabIndex = 21;
            this.CodigoV.Text = "0";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label1.Location = new System.Drawing.Point(255, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(71, 21);
            this.label1.TabIndex = 22;
            this.label1.Text = "Codigo : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label2.Location = new System.Drawing.Point(23, 24);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(77, 21);
            this.label2.TabIndex = 23;
            this.label2.Text = "Nombre : ";
            // 
            // label_name_cliente
            // 
            this.label_name_cliente.AutoSize = true;
            this.label_name_cliente.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label_name_cliente.Location = new System.Drawing.Point(99, 24);
            this.label_name_cliente.Name = "label_name_cliente";
            this.label_name_cliente.Size = new System.Drawing.Size(18, 21);
            this.label_name_cliente.TabIndex = 24;
            this.label_name_cliente.Text = "0";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label3.Location = new System.Drawing.Point(23, 104);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(91, 21);
            this.label3.TabIndex = 25;
            this.label3.Text = "Referencia : ";
            // 
            // label_referencia
            // 
            this.label_referencia.AutoSize = true;
            this.label_referencia.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label_referencia.Location = new System.Drawing.Point(99, 104);
            this.label_referencia.Name = "label_referencia";
            this.label_referencia.Size = new System.Drawing.Size(18, 21);
            this.label_referencia.TabIndex = 26;
            this.label_referencia.Text = "0";
            // 
            // label_tipo_de_pago
            // 
            this.label_tipo_de_pago.AutoSize = true;
            this.label_tipo_de_pago.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label_tipo_de_pago.Location = new System.Drawing.Point(328, 104);
            this.label_tipo_de_pago.Name = "label_tipo_de_pago";
            this.label_tipo_de_pago.Size = new System.Drawing.Size(18, 21);
            this.label_tipo_de_pago.TabIndex = 28;
            this.label_tipo_de_pago.Text = "0";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.label5.Location = new System.Drawing.Point(203, 104);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(111, 21);
            this.label5.TabIndex = 27;
            this.label5.Text = "Tipo de Pago : ";
            // 
            // Detalles_venta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(441, 411);
            this.Controls.Add(this.label_tipo_de_pago);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label_referencia);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label_name_cliente);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CodigoV);
            this.Controls.Add(this.dataGridView_productos);
            this.Controls.Add(this.groupBox1);
            this.Name = "Detalles_venta";
            this.Text = "Detalles_venta";
            this.Load += new System.EventHandler(this.Detalles_venta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_productos)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dataGridView_productos;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label CodigoV;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label_name_cliente;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label_referencia;
        private System.Windows.Forms.Label label_tipo_de_pago;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label_monto_total;
        private System.Windows.Forms.Label label7;
    }
}