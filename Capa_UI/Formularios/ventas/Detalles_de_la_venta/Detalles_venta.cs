﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using principal.ventas;
namespace Capa_UI.Formularios.ventas.Detalles_de_la_venta
{
    public partial class Detalles_venta : Form
    {
        logica_ventas objeto = new logica_ventas();
        public Detalles_venta(string codigo_venta,string nombre, string referencia,string monto,string tipo_p)
        {

            InitializeComponent();
            label_monto_total.Text = monto;
            label_name_cliente.Text = nombre;
            label_referencia.Text = referencia;
            label_tipo_de_pago.Text = tipo_p;
            CodigoV.Text = codigo_venta;
            dataGridView_productos.DataSource = objeto.detalles_venta("select Productos,Cant_Productos as [Cantidad de productos],Monto/Cant_Productos as [Precio por Unidad] , Monto as [Monto Total ]from Tabla_Ventas where codigo_de_venta = '" + codigo_venta+"'");
        }

        private void Detalles_venta_Load(object sender, EventArgs e)
        {

        }
    }
}
