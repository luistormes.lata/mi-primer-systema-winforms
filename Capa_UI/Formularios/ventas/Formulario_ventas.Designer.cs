﻿namespace Capa_UI.Formularios.ventas
{
    partial class Formulario_ventas
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.dataGridView_busqueda = new System.Windows.Forms.DataGridView();
            this.button3 = new System.Windows.Forms.Button();
            this.dateTimeinicio = new System.Windows.Forms.DateTimePicker();
            this.dateTime_final = new System.Windows.Forms.DateTimePicker();
            this.button_cargar = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.dataGridView_30_dias = new System.Windows.Forms.DataGridView();
            this.button2 = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.dataGridView_semana = new System.Windows.Forms.DataGridView();
            this.button1 = new System.Windows.Forms.Button();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.dataGridView_venta = new System.Windows.Forms.DataGridView();
            this.fecha = new System.Windows.Forms.Label();
            this.btn_detalles = new System.Windows.Forms.Button();
            this.Venta = new System.Windows.Forms.TabPage();
            this.tabControl_menu = new System.Windows.Forms.TabControl();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.comboBox_Tipo_pago = new System.Windows.Forms.ComboBox();
            this.textBox_referencia = new System.Windows.Forms.TextBox();
            this.comboBox_cliente = new System.Windows.Forms.ComboBox();
            this.label_Cedula = new System.Windows.Forms.Label();
            this.textBox_Cedula = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.comboBox_producto = new System.Windows.Forms.ComboBox();
            this.textBox_cantidad = new System.Windows.Forms.TextBox();
            this.button_menos = new System.Windows.Forms.Button();
            this.btn_agregar = new System.Windows.Forms.Button();
            this.button_mas = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label_codigo_de_venta = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label_fecha = new System.Windows.Forms.Label();
            this.dataGridView_producto = new System.Windows.Forms.DataGridView();
            this.btn_facturar = new System.Windows.Forms.Button();
            this.btn_limpiar = new System.Windows.Forms.Button();
            this.label10 = new System.Windows.Forms.Label();
            this.label_total_pagar = new System.Windows.Forms.Label();
            this.tabPage4.SuspendLayout();
            this.groupBox7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_busqueda)).BeginInit();
            this.tabPage3.SuspendLayout();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_30_dias)).BeginInit();
            this.tabPage1.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_semana)).BeginInit();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_venta)).BeginInit();
            this.Venta.SuspendLayout();
            this.tabControl_menu.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_producto)).BeginInit();
            this.SuspendLayout();
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.groupBox7);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(801, 454);
            this.tabPage4.TabIndex = 4;
            this.tabPage4.Text = "Buscar por fecha";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.button_cargar);
            this.groupBox7.Controls.Add(this.dateTime_final);
            this.groupBox7.Controls.Add(this.dateTimeinicio);
            this.groupBox7.Controls.Add(this.button3);
            this.groupBox7.Controls.Add(this.dataGridView_busqueda);
            this.groupBox7.Location = new System.Drawing.Point(8, 20);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(785, 414);
            this.groupBox7.TabIndex = 4;
            this.groupBox7.TabStop = false;
            this.groupBox7.Enter += new System.EventHandler(this.groupBox7_Enter);
            // 
            // dataGridView_busqueda
            // 
            this.dataGridView_busqueda.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_busqueda.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_busqueda.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_busqueda.Location = new System.Drawing.Point(17, 89);
            this.dataGridView_busqueda.Name = "dataGridView_busqueda";
            this.dataGridView_busqueda.Size = new System.Drawing.Size(753, 271);
            this.dataGridView_busqueda.TabIndex = 2;
            this.dataGridView_busqueda.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_busqueda_CellMouseClick);
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button3.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.button3.Location = new System.Drawing.Point(17, 366);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(176, 29);
            this.button3.TabIndex = 15;
            this.button3.Text = "Ver detalles de la venta ";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // dateTimeinicio
            // 
            this.dateTimeinicio.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTimeinicio.Location = new System.Drawing.Point(27, 19);
            this.dateTimeinicio.Name = "dateTimeinicio";
            this.dateTimeinicio.Size = new System.Drawing.Size(211, 20);
            this.dateTimeinicio.TabIndex = 16;
            // 
            // dateTime_final
            // 
            this.dateTime_final.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dateTime_final.Location = new System.Drawing.Point(265, 19);
            this.dateTime_final.Name = "dateTime_final";
            this.dateTime_final.Size = new System.Drawing.Size(211, 20);
            this.dateTime_final.TabIndex = 17;
            // 
            // button_cargar
            // 
            this.button_cargar.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.button_cargar.Location = new System.Drawing.Point(496, 16);
            this.button_cargar.Name = "button_cargar";
            this.button_cargar.Size = new System.Drawing.Size(88, 29);
            this.button_cargar.TabIndex = 18;
            this.button_cargar.Text = "Cargar";
            this.button_cargar.UseVisualStyleBackColor = true;
            this.button_cargar.Click += new System.EventHandler(this.button_cargar_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.groupBox6);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(801, 454);
            this.tabPage3.TabIndex = 3;
            this.tabPage3.Text = "Ultimos 30 dias";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.Controls.Add(this.button2);
            this.groupBox6.Controls.Add(this.dataGridView_30_dias);
            this.groupBox6.Location = new System.Drawing.Point(8, 20);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(785, 414);
            this.groupBox6.TabIndex = 4;
            this.groupBox6.TabStop = false;
            // 
            // dataGridView_30_dias
            // 
            this.dataGridView_30_dias.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_30_dias.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_30_dias.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_30_dias.Location = new System.Drawing.Point(17, 49);
            this.dataGridView_30_dias.Name = "dataGridView_30_dias";
            this.dataGridView_30_dias.Size = new System.Drawing.Size(753, 311);
            this.dataGridView_30_dias.TabIndex = 2;
            this.dataGridView_30_dias.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_30_dias_CellMouseClick);
            // 
            // button2
            // 
            this.button2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button2.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.button2.Location = new System.Drawing.Point(17, 366);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(176, 29);
            this.button2.TabIndex = 15;
            this.button2.Text = "Ver detalles de la venta ";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(801, 454);
            this.tabPage1.TabIndex = 2;
            this.tabPage1.Text = "Ultima semana";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.button1);
            this.groupBox5.Controls.Add(this.dataGridView_semana);
            this.groupBox5.Location = new System.Drawing.Point(8, 19);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(785, 414);
            this.groupBox5.TabIndex = 3;
            this.groupBox5.TabStop = false;
            // 
            // dataGridView_semana
            // 
            this.dataGridView_semana.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_semana.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_semana.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_semana.Location = new System.Drawing.Point(17, 48);
            this.dataGridView_semana.Name = "dataGridView_semana";
            this.dataGridView_semana.Size = new System.Drawing.Size(753, 312);
            this.dataGridView_semana.TabIndex = 2;
            this.dataGridView_semana.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_semana_CellClick);
            this.dataGridView_semana.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_semana_CellContentClick);
            this.dataGridView_semana.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dataGridView_semana_CellMouseClick);
            // 
            // button1
            // 
            this.button1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.button1.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.button1.Location = new System.Drawing.Point(17, 366);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(176, 29);
            this.button1.TabIndex = 15;
            this.button1.Text = "Ver detalles de la venta ";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btn_detalles);
            this.tabPage2.Controls.Add(this.fecha);
            this.tabPage2.Controls.Add(this.dataGridView_venta);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(801, 454);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Ventas del dia";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // dataGridView_venta
            // 
            this.dataGridView_venta.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_venta.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_venta.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_venta.Location = new System.Drawing.Point(24, 50);
            this.dataGridView_venta.Name = "dataGridView_venta";
            this.dataGridView_venta.Size = new System.Drawing.Size(753, 349);
            this.dataGridView_venta.TabIndex = 1;
            this.dataGridView_venta.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView_venta_CellContentClick);
            this.dataGridView_venta.CellMouseClick += new System.Windows.Forms.DataGridViewCellMouseEventHandler(this.dgv);
            // 
            // fecha
            // 
            this.fecha.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.fecha.AutoSize = true;
            this.fecha.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.fecha.Location = new System.Drawing.Point(632, 16);
            this.fecha.Name = "fecha";
            this.fecha.Size = new System.Drawing.Size(49, 21);
            this.fecha.TabIndex = 13;
            this.fecha.Text = "Fecha";
            // 
            // btn_detalles
            // 
            this.btn_detalles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_detalles.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.btn_detalles.Location = new System.Drawing.Point(24, 417);
            this.btn_detalles.Name = "btn_detalles";
            this.btn_detalles.Size = new System.Drawing.Size(176, 29);
            this.btn_detalles.TabIndex = 14;
            this.btn_detalles.Text = "Ver detalles de la venta ";
            this.btn_detalles.UseVisualStyleBackColor = true;
            this.btn_detalles.Click += new System.EventHandler(this.btn_detalles_Click);
            // 
            // Venta
            // 
            this.Venta.Controls.Add(this.label_total_pagar);
            this.Venta.Controls.Add(this.label10);
            this.Venta.Controls.Add(this.btn_limpiar);
            this.Venta.Controls.Add(this.btn_facturar);
            this.Venta.Controls.Add(this.dataGridView_producto);
            this.Venta.Controls.Add(this.label_fecha);
            this.Venta.Controls.Add(this.label8);
            this.Venta.Controls.Add(this.label_codigo_de_venta);
            this.Venta.Controls.Add(this.label7);
            this.Venta.Controls.Add(this.groupBox2);
            this.Venta.Controls.Add(this.groupBox1);
            this.Venta.Controls.Add(this.label3);
            this.Venta.Controls.Add(this.groupBox4);
            this.Venta.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.Venta.Location = new System.Drawing.Point(4, 22);
            this.Venta.Name = "Venta";
            this.Venta.Padding = new System.Windows.Forms.Padding(3);
            this.Venta.Size = new System.Drawing.Size(804, 439);
            this.Venta.TabIndex = 0;
            this.Venta.Text = "Vender";
            this.Venta.UseVisualStyleBackColor = true;
            this.Venta.Click += new System.EventHandler(this.Venta_Click);
            // 
            // tabControl_menu
            // 
            this.tabControl_menu.Controls.Add(this.Venta);
            this.tabControl_menu.Controls.Add(this.tabPage2);
            this.tabControl_menu.Controls.Add(this.tabPage1);
            this.tabControl_menu.Controls.Add(this.tabPage3);
            this.tabControl_menu.Controls.Add(this.tabPage4);
            this.tabControl_menu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl_menu.Location = new System.Drawing.Point(0, 0);
            this.tabControl_menu.Name = "tabControl_menu";
            this.tabControl_menu.SelectedIndex = 0;
            this.tabControl_menu.Size = new System.Drawing.Size(812, 465);
            this.tabControl_menu.TabIndex = 1;
            this.tabControl_menu.Tag = "";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.groupBox4.Location = new System.Drawing.Point(17, 20);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(764, 15);
            this.groupBox4.TabIndex = 41;
            this.groupBox4.TabStop = false;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Segoe UI Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(467, 176);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(0, 21);
            this.label3.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label1.Location = new System.Drawing.Point(13, 137);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 19);
            this.label1.TabIndex = 31;
            this.label1.Text = "Referencia : ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label5.Location = new System.Drawing.Point(13, 98);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 19);
            this.label5.TabIndex = 33;
            this.label5.Text = "Tipo de Pago : ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label2.Location = new System.Drawing.Point(14, 33);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(111, 19);
            this.label2.TabIndex = 29;
            this.label2.Text = "Nombre  cliente: ";
            // 
            // comboBox_Tipo_pago
            // 
            this.comboBox_Tipo_pago.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.comboBox_Tipo_pago.FormattingEnabled = true;
            this.comboBox_Tipo_pago.Location = new System.Drawing.Point(152, 98);
            this.comboBox_Tipo_pago.Name = "comboBox_Tipo_pago";
            this.comboBox_Tipo_pago.Size = new System.Drawing.Size(163, 25);
            this.comboBox_Tipo_pago.TabIndex = 37;
            this.comboBox_Tipo_pago.SelectedIndexChanged += new System.EventHandler(this.comboBox_Tipo_pago_SelectedIndexChanged);
            // 
            // textBox_referencia
            // 
            this.textBox_referencia.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.textBox_referencia.Location = new System.Drawing.Point(152, 129);
            this.textBox_referencia.Name = "textBox_referencia";
            this.textBox_referencia.Size = new System.Drawing.Size(163, 25);
            this.textBox_referencia.TabIndex = 39;
            // 
            // comboBox_cliente
            // 
            this.comboBox_cliente.FormattingEnabled = true;
            this.comboBox_cliente.Location = new System.Drawing.Point(152, 28);
            this.comboBox_cliente.Name = "comboBox_cliente";
            this.comboBox_cliente.Size = new System.Drawing.Size(163, 29);
            this.comboBox_cliente.TabIndex = 40;
            this.comboBox_cliente.SelectedIndexChanged += new System.EventHandler(this.comboBox_cliente_SelectedIndexChanged);
            // 
            // label_Cedula
            // 
            this.label_Cedula.AutoSize = true;
            this.label_Cedula.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label_Cedula.Location = new System.Drawing.Point(15, 66);
            this.label_Cedula.Name = "label_Cedula";
            this.label_Cedula.Size = new System.Drawing.Size(61, 19);
            this.label_Cedula.TabIndex = 41;
            this.label_Cedula.Text = "Cedula : ";
            // 
            // textBox_Cedula
            // 
            this.textBox_Cedula.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.textBox_Cedula.Location = new System.Drawing.Point(152, 66);
            this.textBox_Cedula.Name = "textBox_Cedula";
            this.textBox_Cedula.Size = new System.Drawing.Size(163, 25);
            this.textBox_Cedula.TabIndex = 42;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.textBox_Cedula);
            this.groupBox1.Controls.Add(this.label_Cedula);
            this.groupBox1.Controls.Add(this.comboBox_cliente);
            this.groupBox1.Controls.Add(this.textBox_referencia);
            this.groupBox1.Controls.Add(this.comboBox_Tipo_pago);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.groupBox1.Location = new System.Drawing.Point(17, 50);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(383, 161);
            this.groupBox1.TabIndex = 34;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Datos para la venta";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label6.Location = new System.Drawing.Point(18, 63);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(73, 19);
            this.label6.TabIndex = 35;
            this.label6.Text = "Cantidad : ";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label4.Location = new System.Drawing.Point(17, 30);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(74, 19);
            this.label4.TabIndex = 34;
            this.label4.Text = "Producto : ";
            // 
            // comboBox_producto
            // 
            this.comboBox_producto.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.comboBox_producto.FormattingEnabled = true;
            this.comboBox_producto.Location = new System.Drawing.Point(128, 27);
            this.comboBox_producto.Name = "comboBox_producto";
            this.comboBox_producto.Size = new System.Drawing.Size(140, 25);
            this.comboBox_producto.TabIndex = 36;
            // 
            // textBox_cantidad
            // 
            this.textBox_cantidad.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.textBox_cantidad.Location = new System.Drawing.Point(128, 63);
            this.textBox_cantidad.Name = "textBox_cantidad";
            this.textBox_cantidad.Size = new System.Drawing.Size(140, 25);
            this.textBox_cantidad.TabIndex = 39;
            // 
            // button_menos
            // 
            this.button_menos.Location = new System.Drawing.Point(274, 63);
            this.button_menos.Name = "button_menos";
            this.button_menos.Size = new System.Drawing.Size(31, 29);
            this.button_menos.TabIndex = 40;
            this.button_menos.Text = "-";
            this.button_menos.UseVisualStyleBackColor = true;
            this.button_menos.Click += new System.EventHandler(this.button_menos_Click);
            // 
            // btn_agregar
            // 
            this.btn_agregar.Location = new System.Drawing.Point(238, 114);
            this.btn_agregar.Name = "btn_agregar";
            this.btn_agregar.Size = new System.Drawing.Size(95, 34);
            this.btn_agregar.TabIndex = 42;
            this.btn_agregar.Text = "Agregar";
            this.btn_agregar.UseVisualStyleBackColor = true;
            this.btn_agregar.Click += new System.EventHandler(this.btn_agregar_Click);
            // 
            // button_mas
            // 
            this.button_mas.Location = new System.Drawing.Point(311, 63);
            this.button_mas.Name = "button_mas";
            this.button_mas.Size = new System.Drawing.Size(28, 29);
            this.button_mas.TabIndex = 41;
            this.button_mas.Text = "+";
            this.button_mas.UseVisualStyleBackColor = true;
            this.button_mas.Click += new System.EventHandler(this.button_mas_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.button_mas);
            this.groupBox2.Controls.Add(this.btn_agregar);
            this.groupBox2.Controls.Add(this.button_menos);
            this.groupBox2.Controls.Add(this.textBox_cantidad);
            this.groupBox2.Controls.Add(this.comboBox_producto);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Font = new System.Drawing.Font("Segoe UI Light", 12F);
            this.groupBox2.Location = new System.Drawing.Point(413, 56);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(368, 155);
            this.groupBox2.TabIndex = 35;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Productos";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label7.Location = new System.Drawing.Point(522, 20);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(122, 19);
            this.label7.TabIndex = 37;
            this.label7.Text = "Codigo de venta :  ";
            // 
            // label_codigo_de_venta
            // 
            this.label_codigo_de_venta.AutoSize = true;
            this.label_codigo_de_venta.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label_codigo_de_venta.Location = new System.Drawing.Point(647, 20);
            this.label_codigo_de_venta.Name = "label_codigo_de_venta";
            this.label_codigo_de_venta.Size = new System.Drawing.Size(29, 19);
            this.label_codigo_de_venta.TabIndex = 38;
            this.label_codigo_de_venta.Text = "AT ";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label8.Location = new System.Drawing.Point(20, 20);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(62, 19);
            this.label8.TabIndex = 39;
            this.label8.Text = "Fecha :   ";
            // 
            // label_fecha
            // 
            this.label_fecha.AutoSize = true;
            this.label_fecha.Font = new System.Drawing.Font("Segoe UI Light", 10F);
            this.label_fecha.Location = new System.Drawing.Point(88, 20);
            this.label_fecha.Name = "label_fecha";
            this.label_fecha.Size = new System.Drawing.Size(32, 19);
            this.label_fecha.TabIndex = 40;
            this.label_fecha.Text = "///  ";
            // 
            // dataGridView_producto
            // 
            this.dataGridView_producto.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView_producto.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView_producto.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView_producto.Location = new System.Drawing.Point(17, 243);
            this.dataGridView_producto.Name = "dataGridView_producto";
            this.dataGridView_producto.Size = new System.Drawing.Size(764, 123);
            this.dataGridView_producto.TabIndex = 52;
            // 
            // btn_facturar
            // 
            this.btn_facturar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btn_facturar.Location = new System.Drawing.Point(626, 379);
            this.btn_facturar.Name = "btn_facturar";
            this.btn_facturar.Size = new System.Drawing.Size(95, 29);
            this.btn_facturar.TabIndex = 50;
            this.btn_facturar.Text = "Facturar";
            this.btn_facturar.UseVisualStyleBackColor = true;
            this.btn_facturar.Click += new System.EventHandler(this.btn_facturar_Click_1);
            // 
            // btn_limpiar
            // 
            this.btn_limpiar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btn_limpiar.Location = new System.Drawing.Point(26, 379);
            this.btn_limpiar.Name = "btn_limpiar";
            this.btn_limpiar.Size = new System.Drawing.Size(94, 29);
            this.btn_limpiar.TabIndex = 51;
            this.btn_limpiar.Text = "Limpiar";
            this.btn_limpiar.UseVisualStyleBackColor = true;
            this.btn_limpiar.Click += new System.EventHandler(this.btn_limpiar_Click_1);
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Segoe UI Light", 14F);
            this.label10.Location = new System.Drawing.Point(290, 379);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(136, 25);
            this.label10.TabIndex = 53;
            this.label10.Text = "Total a pagar  : ";
            // 
            // label_total_pagar
            // 
            this.label_total_pagar.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label_total_pagar.AutoSize = true;
            this.label_total_pagar.Font = new System.Drawing.Font("Segoe UI Light", 14F);
            this.label_total_pagar.Location = new System.Drawing.Point(415, 379);
            this.label_total_pagar.Name = "label_total_pagar";
            this.label_total_pagar.Size = new System.Drawing.Size(22, 25);
            this.label_total_pagar.TabIndex = 54;
            this.label_total_pagar.Text = "0";
            // 
            // Formulario_ventas
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(812, 465);
            this.Controls.Add(this.tabControl_menu);
            this.Name = "Formulario_ventas";
            this.Text = "Formulario_ventas";
            this.tabPage4.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_busqueda)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_30_dias)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_semana)).EndInit();
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_venta)).EndInit();
            this.Venta.ResumeLayout(false);
            this.Venta.PerformLayout();
            this.tabControl_menu.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView_producto)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button button_cargar;
        private System.Windows.Forms.DateTimePicker dateTime_final;
        private System.Windows.Forms.DateTimePicker dateTimeinicio;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.DataGridView dataGridView_busqueda;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.DataGridView dataGridView_30_dias;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.DataGridView dataGridView_semana;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.Button btn_detalles;
        private System.Windows.Forms.Label fecha;
        private System.Windows.Forms.DataGridView dataGridView_venta;
        private System.Windows.Forms.TabPage Venta;
        private System.Windows.Forms.TabControl tabControl_menu;
        private System.Windows.Forms.Label label_total_pagar;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Button btn_limpiar;
        private System.Windows.Forms.Button btn_facturar;
        private System.Windows.Forms.DataGridView dataGridView_producto;
        private System.Windows.Forms.Label label_fecha;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label_codigo_de_venta;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button_mas;
        private System.Windows.Forms.Button btn_agregar;
        private System.Windows.Forms.Button button_menos;
        private System.Windows.Forms.TextBox textBox_cantidad;
        private System.Windows.Forms.ComboBox comboBox_producto;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox textBox_Cedula;
        private System.Windows.Forms.Label label_Cedula;
        private System.Windows.Forms.ComboBox comboBox_cliente;
        private System.Windows.Forms.TextBox textBox_referencia;
        private System.Windows.Forms.ComboBox comboBox_Tipo_pago;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox4;
    }
}