﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using principal.ventas;
using Capa_UI.Formularios.ventas.Detalles_de_la_venta;
namespace Capa_UI.Formularios.ventas
{
    public partial class Formulario_ventas : Form
    {
        logica_ventas objeto = new logica_ventas();
        Facturar_venta NuevaFactura = new Facturar_venta();


        private DataTable dt;
        public Formulario_ventas()
        {
            InitializeComponent();
            llenar_combobox();
            vender();
            ventas_por_periodo();
            fecha.Text = DateTime.Today.ToString();
            comboBox_cliente.Text = "";


        }
        string nombre_cliente = "";
        string codigo_de_venta = "";
        string referencia = "";
        string Monto = "";
        string Tipo_p = "";
        private void btn_detalles_Click(object sender, EventArgs e)
        {
            llamar_detalles_con_boton();
           
        }
        
        private void dgv(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewRow fila = dataGridView_venta.Rows[e.RowIndex];
            captar_evento_mouseclik_datagrid(fila);
        }
   
        private void formulario_hijo(Form hijo)
        {
            hijo.BringToFront();
            hijo.Show();
        }

        List<string> lista_Tipo_pago = new List<string>();
        List<string> lista_precio = new List<string>();
        public void llenar_combobox()
        {
            lista_Tipo_pago.Add("Punto");
            lista_Tipo_pago.Add("Transferencia");
            lista_Tipo_pago.Add("Biopago");
            lista_Tipo_pago.Add("Efectivo");
            lista_Tipo_pago.Add("Pago Movil");
            lista_Tipo_pago.Add("Divisa");
            comboBox_Tipo_pago.DataSource = lista_Tipo_pago;
            comboBox_producto.DataSource = objeto.mostrar_lista("select Nombre from productos");
            comboBox_cliente.DataSource = objeto.mostrar_lista("select Nombre +' '+ Apellido from Clientes");
        }

        public void codigo_para_la_venta()
        {
            Random random = new Random();
            int num = random.Next(10000, 99999);
            label_codigo_de_venta.Text = "AT"+ Convert.ToString(num);
        }
        public void vender()
        {


            codigo_para_la_venta();
            label_fecha.Text = DateTime.Today.ToString();
            textBox_cantidad.Text = "1";

            dt = new DataTable();
            dt.Columns.Add("Producto");
            dt.Columns.Add("Cantidad");
            dt.Columns.Add("Precio por unidad");
            dt.Columns.Add("Precio Total");
            dataGridView_producto.DataSource = dt;


        }

        private void button_mas_Click(object sender, EventArgs e)
        {
            int sum = Convert.ToInt32(textBox_cantidad.Text) + 1;
            textBox_cantidad.Text = Convert.ToString(sum);
        }

        private void button_menos_Click(object sender, EventArgs e)
        {
            if(Convert.ToInt32(textBox_cantidad.Text) > 0)
            {
                int menos = Convert.ToInt32(textBox_cantidad.Text) - 1;
                textBox_cantidad.Text = Convert.ToString(menos);
            }
          
        }

        private void Venta_Click(object sender, EventArgs e)
        {

        }

        /*boton para efectuar la venta*/
        private void btn_facturar_Click(object sender, EventArgs e)
        {

       

        }
        int total,num;
        private void btn_agregar_Click(object sender, EventArgs e)
        {
            string query = "select precio from productos where Nombre = '" + comboBox_producto.Text + "'";
            lista_precio = objeto.mostrar_lista(query);
            num = Convert.ToInt32(lista_precio[0]) * Convert.ToInt32(textBox_cantidad.Text);
            
            DataRow fila = dt.NewRow();
            fila["Producto"] = comboBox_producto.Text;
            fila["Cantidad"] = textBox_cantidad.Text;
            fila["Precio por unidad"] = lista_precio[0]; 
            fila["Precio Total"] = num; 
            dt.Rows.Add(fila);

            total = total + num;
            label_total_pagar.Text = Convert.ToString(total);
        }

        private void dataGridView_venta_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void btn_limpiar_Click(object sender, EventArgs e)
        {
            dt.Rows.Clear();
            
        }

        private void btn_facturar_Click_1(object sender, EventArgs e)
        {
            bool seinserto = false;
            foreach (DataRow fila in dt.Rows)
            {

                string cantidad = fila["Cantidad"].ToString();
                string monto = fila["Precio Total"].ToString();
                string producto = fila["Producto"].ToString();

                seinserto = NuevaFactura.insertar(label_codigo_de_venta.Text, comboBox_cliente.Text, cantidad, producto, label_fecha.Text, textBox_referencia.Text, monto, comboBox_Tipo_pago.Text,textBox_Cedula.Text);
            }

            if (seinserto == true)
            {
                MessageBox.Show("se ha insertado correptamente");
                label_codigo_de_venta.Text = "";
                codigo_para_la_venta();
                ventas_por_periodo();
            }

            else
                MessageBox.Show("Ha ocurrido un error");

        }
        
        #region nulo
        private void groupBox7_Enter(object sender, EventArgs e)
        {

        }

       
       private void dataGridView_semana_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void dataGridView_semana_CellClick(object sender, DataGridViewCellEventArgs e)
        {

        }
        #endregion
        private void button_cargar_Click(object sender, EventArgs e)
        {
            var fechainicio = Convert.ToString(dateTimeinicio.Value.AddDays(-1));
            var fechafinal = Convert.ToString(dateTime_final.Value);
            dataGridView_busqueda.DataSource = objeto.mostrar_por_periodo(fechainicio, fechafinal);

        }

        private void dataGridView_semana_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewRow fila = dataGridView_semana.Rows[e.RowIndex];
            captar_evento_mouseclik_datagrid(fila);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            llamar_detalles_con_boton();
        }

        private void dataGridView_30_dias_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewRow fila = dataGridView_30_dias.Rows[e.RowIndex];
            captar_evento_mouseclik_datagrid(fila);
        }
        private void button2_Click(object sender, EventArgs e)
        {
            llamar_detalles_con_boton();
        }
        private void dataGridView_busqueda_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewRow fila = dataGridView_busqueda.Rows[e.RowIndex];
            captar_evento_mouseclik_datagrid(fila);
        }

        public void ventas_por_periodo()
        {
            //ventas hoy //

            var fechainicio = DateTime.Today.ToString();
            string fechafinal = DateTime.Now.ToString();
            dataGridView_venta.DataSource = objeto.mostrar_por_periodo(fechainicio, fechafinal);

            // ultima semana

            fechainicio = Convert.ToString(DateTime.Today.AddDays(-7));
            
            dataGridView_semana.DataSource = objeto.mostrar_por_periodo(fechainicio,fechafinal);

            //ultimo mes

            fechainicio = Convert.ToString(new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1));
            dataGridView_30_dias.DataSource = objeto.mostrar_por_periodo(fechainicio, fechafinal);
        }

        public void captar_evento_mouseclik_datagrid(DataGridViewRow fila)
        {
            nombre_cliente = Convert.ToString(fila.Cells[0].Value);
            referencia = Convert.ToString(fila.Cells[1].Value);
            codigo_de_venta = Convert.ToString(fila.Cells[2].Value);
            Monto = Convert.ToString(fila.Cells[4].Value);
            Tipo_p = Convert.ToString(fila.Cells[5].Value);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            llamar_detalles_con_boton();
        }

        private void btn_limpiar_Click_1(object sender, EventArgs e)
        {
            comboBox_cliente.Text = "";            
            textBox_cantidad.Text = "0";
            textBox_referencia.Text = "";
            textBox_Cedula.Text = "";
            dt.Rows.Clear();
        }

        private void comboBox_Tipo_pago_SelectedIndexChanged(object sender, EventArgs e)
        {
           
        }

        private void comboBox_cliente_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<string> lista = new List<string>();
            string query = "select cedula from clientes where  Nombre +' '+ Apellido = '" + comboBox_cliente.Text+"'";
            lista = objeto.mostrar_lista(query);
            textBox_Cedula.Text = lista[0];
        }

        public void llamar_detalles_con_boton()
        {
            if (codigo_de_venta != "")
            {
                formulario_hijo(new Detalles_venta(codigo_de_venta, nombre_cliente, referencia, Monto, Tipo_p));

                nombre_cliente = "";
                codigo_de_venta = "";
                referencia = "";
                Monto = "";
                Tipo_p = "";
            }
            else
            {
                MessageBox.Show("no hay venta seleccionada");
            }
        }
    }

    
}
