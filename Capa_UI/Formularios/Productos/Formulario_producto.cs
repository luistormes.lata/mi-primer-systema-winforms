﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using principal.productos;
namespace Capa_UI.Formularios.Productos
{
    public partial class Formulario_producto : Form
    {
        logica_producto objeto = new logica_producto();

        public Formulario_producto()
        {
            InitializeComponent();
            cargar_datos();
        }
        public void cargar_datos()
        {
            dataGridView_producto.DataSource = objeto.mostrar("select * from Productos");
        }

        private void button_limpiar_Click(object sender, EventArgs e)
        {
            textBox_nombre.Text = "";
            textBox_precio.Text = "";
        }

        private void button_agregar_Click(object sender, EventArgs e)
        {
            string mensage =  objeto.insertar(textBox_nombre.Text, textBox_precio.Text);
            MessageBox.Show(mensage);
            cargar_datos();
        }
        string id_producto = "";
        string Nombre = "";
        string Precio = "";
        private void dataGridView_producto_CellMouseClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            DataGridViewRow fila = dataGridView_producto.Rows[e.RowIndex];
            id_producto= Convert.ToString(fila.Cells[0].Value);
            Nombre = Convert.ToString(fila.Cells[1].Value);
            Precio = Convert.ToString(fila.Cells[2].Value);
        }

        private void button_eliminR_Click(object sender, EventArgs e)
        {
            if (id_producto!= "")
            {
                string mensage = objeto.delete(string.Format("delete from productos where id_producto = {0}", id_producto));
                MessageBox.Show(mensage);
                id_producto= "";
                cargar_datos();
            }
            else
            {
                MessageBox.Show("no hay producto seleccionado");
            }
        }

        private void button_editar_Click(object sender, EventArgs e)
        {
            if (id_producto!= "")
            {
                button_update.Visible = true;
                button_agregar.Visible = false;
                textBox_nombre.Text = Nombre;
                textBox_precio.Text = Precio;
            }
            else
            {
                MessageBox.Show("no hay producto seleccionado");
            }
        }

        private void button_update_Click(object sender, EventArgs e)
        {
            string mensage =objeto.update(string.Format("update productos set Nombre ='{0}',precio = '{1}' where id_producto = {2}", new string[] { textBox_nombre.Text, textBox_precio.Text,id_producto }));
            MessageBox.Show(mensage);
            button_update.Visible = false;
            button_agregar.Visible = true;
            textBox_nombre.Text = "";
            textBox_precio.Text = "";
            id_producto = "";
            cargar_datos();

        }
    }
}
